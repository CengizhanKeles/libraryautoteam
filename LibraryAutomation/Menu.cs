﻿using System;
using System.Data;
using System.Drawing;
using System.Data.SqlClient;
using System.Windows.Forms;
using LibraryAutomation.Properties;
using MessagingToolkit.QRCode.Codec;
using System.Net.Mail;
//using MessagingToolkit.QRCode.Codec;
namespace LibraryAutomation
{
    public partial class Menu : Form
    {
        SqlConnection conn = new SqlConnection(@"Data source=.\SQLEXPRESS;Database=Kutuphane;Integrated Security=true;");

        private bool isCollapsed = true;
        private bool isCollapsed2 = true;
        private bool isCollapsed3 = true;

        public Menu()
        {
            InitializeComponent();
        }
        int kitapSayisi = -1
            , kayitSayisi = -1
            , prop = -1
            , emanet = -1;
        public int YetkiliID { get; set; }
        public void Yetkilendirme()
        {

            int YetkiID = 0;
            SqlCommand cmd = new SqlCommand("select Name,Permission.ID,PermissionName  from UserLogin inner join Permission on UserLogin.PerID = Permission.ID Where UserLogin.ID = @id", conn);
            cmd.Parameters.AddWithValue("@id", YetkiliID);
            conn.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                lblKullaniciDurumuText.Text = dr[0].ToString();
                lblYetkidurumuText.Text = dr[2].ToString();
                YetkiID = Convert.ToInt32(dr[1]);
            }
            if (YetkiID == 1)
            {
                //btnKullaniciEkle.Enabled = true;
                BtnIstatistikler.Enabled = true;
                BtnKitap.Enabled = true;
                BtnKitapEkle.Enabled = true;
                BtnEnvanter.Enabled = true;
                //  BtnOnerilenler.Enabled = true;
                groupBox1.Enabled = true;
                groupBox2.Enabled = true;

            }
            else if (YetkiID == 2)
            {
                // btnKullaniciEkle.Visible = false;
                BtnIstatistikler.Enabled = true;
                BtnKitap.Enabled = true;
                BtnKitapEkle.Enabled = true;
                BtnEnvanter.Enabled = true;
                //   BtnOnerilenler.Enabled = true;
                groupBox1.Enabled = true;
                groupBox2.Enabled = true;
                btnkllanıcı.Visible = false;


            }
            else if (YetkiID == 3)
            {
                //btnKullaniciEkle.Visible = false;
                BtnIstatistikler.Enabled = true;
                BtnKitap.Enabled = false;
                BtnKitapEkle.Enabled = false;
                BtnEnvanter.Enabled = true;
                //BtnOnerilenler.Enabled = false;
                groupBox1.Enabled = false;
                groupBox2.Enabled = true;
                kitapislm.Visible = false;
                btnkllanıcı.Visible = false;

            }
            conn.Close();
        }
        private Image KareKodOlustur(string giris, int kkDuzey)
        {
            var deger = giris;
            MessagingToolkit.QRCode.Codec.QRCodeEncoder qe = new MessagingToolkit.QRCode.Codec.QRCodeEncoder();
            qe.QRCodeEncodeMode = QRCodeEncoder.ENCODE_MODE.BYTE;
            qe.QRCodeErrorCorrect = QRCodeEncoder.ERROR_CORRECTION.L;
            qe.QRCodeVersion = kkDuzey;
            System.Drawing.Bitmap bm = qe.Encode(deger);
            return bm;
        }
        private void Menu_Load(object sender, EventArgs e)
        {
            txtbsmyılı.MaxLength = 4;
            txtKitapAdi.MaxLength =100;
            txtQR.MaxLength = 8;
            cmbKitapTur.MaxLength = 100;
            txtSayfaSayisi.MaxLength = 4;
            txtYayinevi.MaxLength = 50;
            txtKitapYazar.MaxLength = 50;
            rtxtOzet.MaxLength = 250;
            // LblYetkiD.Text = YetkiliID.ToString();
            Istatistikler();
            Giris grs = new Giris();
            txtSayfaSayisi.MaxLength = 4;
            cmbKitapTur.SelectedIndex = 0;
            string GuidKey = Guid.NewGuid().ToString().Substring(0, Guid.NewGuid().ToString().IndexOf("-"));
            txtQR.Text = GuidKey.ToString();
            try
            {
                pcbQR.Image = KareKodOlustur(txtQR.Text, 4);
                pcbQR.Visible = true;
            }
            catch
            {
                MessageBox.Show("URL adresiniz için Kare kod oluşturulamadı.", "Hata !", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            getirSuresiDolanlar();
            getirSuresiGecmisler();

            //LblKullaniciAdi.Text = grs.ad.ToString();
        }
       public string bugun = DateTime.Now.Date.ToString("yyyy-MM-dd");
        private void getirSuresiGecmisler()
        {
            //lvgecmis.Items.Clear();               
            SqlCommand cmd = new SqlCommand("select t2.BookName, t3.Adı, t1.Date, t1.Takeit,t1.Durum from Brow t1 inner join Book t2 on (t2.ID = t1.BookID) inner join User_tbl t3 on (t3.ID = t1.UserID) where (Takeit<@1) AND t1.Durum=0", conn);
            cmd.Parameters.AddWithValue("@1", bugun);
            conn.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                var d1 = dr[2];
                var d2 = dr[3];
                ListViewItem item = new ListViewItem(dr[0].ToString());
                item.SubItems.Add(dr[1].ToString());
                item.SubItems.Add(dr[2].ToString());
                item.SubItems.Add(dr[3].ToString());
                lvgecmis.Items.Add(item);
            }
            dr.Close();
            conn.Close();
        }
        private void getirSuresiDolanlar()
        {
            string yarin = DateTime.Now.Date.AddDays(1).ToString("yyyy-MM-dd");
            //lvBugun.Items.Clear();
            SqlCommand cmd = new SqlCommand("select t2.BookName, t3.Adı, t1.Date, t1.Takeit,t1.Durum from Brow t1 inner join Book t2 on (t2.ID = t1.BookID) inner join User_tbl t3 on (t3.ID = t1.UserID) where (Takeit =@1 or Takeit =@2) AND t1.Durum=0", conn);         
            cmd.Parameters.AddWithValue("@1", bugun);
            cmd.Parameters.AddWithValue("@2", yarin);
            conn.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                ListViewItem item = new ListViewItem(dr[0].ToString());
                item.SubItems.Add(dr[1].ToString());
                item.SubItems.Add(dr[2].ToString());
                item.SubItems.Add(dr[3].ToString());
                lvBugun.Items.Add(item);
            }
            dr.Close();
            conn.Close();
        }
        private void Istatistikler()
        {

            if (conn.State == ConnectionState.Closed)
            {

            }

            {

                try
                {
                    groupBox2.Visible = true;
                    BtnIstatistikler.Enabled = false;
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("select count(*) from Book where durum=0", conn);
                    kitapSayisi = Convert.ToInt32(cmd.ExecuteScalar());
                    conn.Close();
                    label2.Text = "Toplam Kitap Sayısı : " + kitapSayisi.ToString();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("" + ex);
                }
                try
                {

                    conn.Open();
                    SqlCommand cmd = new SqlCommand("select count(*) from Brow where durum=0", conn);
                    emanet = Convert.ToInt32(cmd.ExecuteScalar());
                    conn.Close();
                    label17.Text = "Emanette Olan Kitap Sayısı : " + emanet.ToString();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("" + ex);
                }
                try
                {

                    conn.Open();
                    SqlCommand cmd = new SqlCommand("select count(*) from User_tbl where PerID=2 and durum=0", conn);
                    kayitSayisi = Convert.ToInt32(cmd.ExecuteScalar());
                    conn.Close();
                    label1.Text = "Toplam Öğretmen Sayısı : " + kayitSayisi.ToString();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("" + ex);
                }
                try
                {

                    conn.Open();
                    SqlCommand cmd = new SqlCommand("select count(*) from User_tbl where PerID=3 and durum=0", conn);
                    kayitSayisi = Convert.ToInt32(cmd.ExecuteScalar());
                    conn.Close();
                    label3.Text = "Toplam Öğrenci Sayısı : " + kayitSayisi.ToString();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("" + ex);
                }


            }
        }
        private void PictureCikis_Click(object sender, EventArgs e)
        {
            DialogResult dialog = new DialogResult();
            dialog = MessageBox.Show("Programdan Çıkmak İstediğinize Emin misiniz ?", "Çıkış", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
            if (dialog == DialogResult.Yes)
            {
                Application.Exit();
            }
        }
        private void PictureKucult_Click(object sender, EventArgs e)
        {
            this.WindowState = System.Windows.Forms.FormWindowState.Minimized;

        }
        private void BtnKitap_Click(object sender, EventArgs e)
        {
            KitapSilListele ksl = new KitapSilListele();
            ksl.Show();
            this.Visible = false;
        }
        private void BtnKitapEkle_Click(object sender, EventArgs e)
        {
            groupBox2.Visible = false;
            BtnIstatistikler.Enabled = true;
            grbboxgecmis.Visible = false;
            GrpboxBgn.Visible = false;
        }
        private void BtnEnvanter_Click(object sender, EventArgs e)
        {
            EmanetVer eva = new EmanetVer();

            eva.Show();
            this.Visible = false;
        }
        private void Button6_Click_1(object sender, EventArgs e)
        {
            if (conn.State == ConnectionState.Closed)
            {
                conn.Open();
            }
            try
            {
                if (cmbKitapTur.SelectedIndex != 0)
                {

                    string query = "insert into Book(BookName,BookAuthor,PageNumber,Publisher,BookSummary,YearPrint,BookType,QR,Durum)values(@1,@2,@3,@4,@5,@6,@7,@8,@9)";
                    SqlCommand cmd = new SqlCommand(query, conn);
                    cmd.Parameters.AddWithValue("@1", txtKitapAdi.Text);
                    cmd.Parameters.AddWithValue("@2", txtKitapYazar.Text);
                    cmd.Parameters.AddWithValue("@3", txtSayfaSayisi.Text);
                    cmd.Parameters.AddWithValue("@4", txtYayinevi.Text);
                    cmd.Parameters.AddWithValue("@5", rtxtOzet.Text);
                    cmd.Parameters.AddWithValue("@6", txtbsmyılı.Text);
                    cmd.Parameters.AddWithValue("@7", cmbKitapTur.Text);
                    cmd.Parameters.AddWithValue("@8", txtQR.Text);
                    cmd.Parameters.AddWithValue("@9", 0);



                    cmd.ExecuteNonQuery();
                    conn.Close();
                    MessageBox.Show("Kitap Başarıyla Eklendi", "Bilgi", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtKitapAdi.Clear();
                    txtKitapYazar.Clear();
                    txtSayfaSayisi.Clear();
                    txtYayinevi.Clear();
                    txtbsmyılı.Clear();
                    rtxtOzet.Clear();
                    cmbKitapTur.SelectedIndex = 0;
                    string GuidKey = Guid.NewGuid().ToString().Substring(0, Guid.NewGuid().ToString().IndexOf("-"));
                    txtQR.Text = GuidKey.ToString();



                }
                else
                {
                    MessageBox.Show("Lütfen Kitap Türünü Seçiniz !", " Hata", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show("Lütfen Bilgileri Kontrol Ediniz" + ex, "Hata", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        bool formTasiniyor = false;
        Point baslangicNoktasi = new Point(0, 0);
        private void PanelUst_MouseDown(object sender, MouseEventArgs e)
        {
            formTasiniyor = true;
            baslangicNoktasi = new Point(e.X, e.Y);
        }
        private void RtxtOzet_TextChanged(object sender, EventArgs e)
        {
            int u = rtxtOzet.TextLength;
            lblktpozeti.Text = "250/" + rtxtOzet.TextLength.ToString();

        }
        private void TxtSayfaSayisi_TextChanged(object sender, EventArgs e)
        {
            int u = txtSayfaSayisi.TextLength;
            lblsayfasysi.Text = "4/" + txtSayfaSayisi.TextLength.ToString();

        }
        private void TxtSayfaSayisi_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                e.Handled = true;
            }

            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }
        private void Button1_Click(object sender, EventArgs e)
        {
            Sistem_Yoneticisi sy = new Sistem_Yoneticisi();
            sy.Show();
            this.Visible = false;
        }

        private void BtnOnerilenler_Click(object sender, EventArgs e)
        {
            Mail ml = new Mail();
            ml.Show();
            this.Visible = false;
        }
        private void Timer1_Tick(object sender, EventArgs e)
        {
            if (isCollapsed)
            {
                kitapislm.Image = Resources.chevron_arrow_up;
                DropDawnPanel.Height += 20;
                if (DropDawnPanel.Size == DropDawnPanel.MaximumSize)
                {
                    timer1.Stop();
                    isCollapsed = false;
                }
            }
            else
            {
                kitapislm.Image = Resources.chevron_arrow_down;
                DropDawnPanel.Height -= 20;
                if (DropDawnPanel.Size == DropDawnPanel.MinimumSize)
                {
                    timer1.Stop();
                    isCollapsed = true;
                }
                // emanetislm.Location = new Point(1, 403);
            }
        }
        int ct = 0;
        private void Menuislm_Click(object sender, EventArgs e)
        {
            timer1.Start();
            if (ct == 0)
            {
                DropDawnPanel2.Location = new Point(1, 553);
                ct = 1;
            }
            else
            {
                DropDawnPanel2.Location = new Point(1, 397);
                ct = 0;
            }
        }
        private void Button1_Click_1(object sender, EventArgs e)
        {
            try
            {
                SqlConnection conn = new SqlConnection(@"Data source=.\;Database=.;Integrated Security=true;");
                // SqlConnection conn = new SqlConnection(@"Data source=.\SQLEXPRESS;Database=.;Integrated Security=true;");
                conn.Open();
                SqlCommand cmd3 = new SqlCommand("select Book.ID,Book.QR,Book.BookName,Book.BookAuthor,User_tbl.ID,User_tbl.Name,User_tbl.LastName,User_tbl.Mail,Brow.ID,Brow.UserID,Brow.BookID,Brow.Date,Brow.Takeit from Brow inner join Book on (Book.ID = Brow.BookID) inner join User_tbl on (User_tbl.ID = Brow.UserID) where Brow_Tbl.Takeit BETWEEN @tr1 and @tr2", conn);
                // cmd3.Parameters.AddWithValue("tr1", dateTimePicker1.Value);
                // cmd3.Parameters.AddWithValue("tr2", dateTimePicker2.Value);
                SqlDataReader dr = cmd3.ExecuteReader();
                while (dr.Read())
                {
                    ListViewItem item = new ListViewItem(dr["BookName"].ToString());
                    item.SubItems.Add(dr["ID"].ToString());
                    item.SubItems.Add(dr["BookType"].ToString());
                    item.SubItems.Add(dr["BookAuthor"].ToString());
                    item.SubItems.Add(dr["PageNumber"].ToString());
                    item.SubItems.Add(dr["Publisher"].ToString());
                    item.SubItems.Add(dr["BookSummary"].ToString());
                    item.SubItems.Add(dr["YearPrint"].ToString());
                    item.SubItems.Add(dr["QR"].ToString());
                    lvBugun.Items.Add(item);

                }
                dr.Close();
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("" + ex);
            }
        }
        private void Button4_Click(object sender, EventArgs e)
        {
            timer2.Start();
        }

        private void Timer2_Tick(object sender, EventArgs e)
        {
            if (isCollapsed2)
            {
                emanetislm.Image = Resources.chevron_arrow_up;
                DropDawnPanel2.Height += 20;
                if (DropDawnPanel2.Size == DropDawnPanel2.MaximumSize)
                {
                    timer2.Stop();
                    isCollapsed2 = false;
                }
            }
            else
            {
                emanetislm.Image = Resources.chevron_arrow_down;
                DropDawnPanel2.Height -= 20;
                if (DropDawnPanel2.Size == DropDawnPanel2.MinimumSize)
                {
                    timer2.Stop();
                    isCollapsed2 = true;
                }
            }
        }
        private void Btnkitapgclle_Click(object sender, EventArgs e)
        {
            KitapGuncelle kg = new KitapGuncelle();
            kg.Show();
            this.Visible = false;
        }

        private void Btnkllanıcı_Click(object sender, EventArgs e)
        {
            Sistem_Yoneticisi sy = new Sistem_Yoneticisi();
            sy.Show();
            this.Hide();
        }
        private void Btnemanetal_Click(object sender, EventArgs e)
        {
            EmanetAl menu = new EmanetAl();
            // menu.id = YetkiliID;
            menu.Show();
            this.Hide();
        }

        private void GroupBox2_Enter(object sender, EventArgs e)
        {
             
        }

        private void LvBugun_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void PanelSol_Paint(object sender, PaintEventArgs e)
        {

        }

        string ad;
        string mail;
        string kitapad;
        DateTime takeit;
        DateTime islem;
        private void buKullanıcıyaMailGönderToolStripMenuItem_Click(object sender, EventArgs e)
        {


            try
            {
                DialogResult result = MessageBox.Show("Bu kullanıcıya Emanet Teslimi Hakkında Mail Gönderilsin mi ?", "E-Mail ", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (result == DialogResult.Yes)
                {
                    conn.Open();
                    MailMessage ePosta = new MailMessage();
                    ePosta.From = new MailAddress("kutuphaneoto.ko@gmail.com");
                    ePosta.To.Add(mail);
                    ePosta.Subject = "Değerli Kullanıcımız Aldığınız Emanet Hakkında Hatırlatma Yapmak İstedik.";
                    ePosta.Body = "" + ad.ToString() + " İsimli Değerli Okuyucumuz " + islem.ToShortDateString() + " Tarihinde Aldığınız " + kitapad.ToString() + " Adlı Kitapın " + takeit.ToShortDateString() + " Tarihinde Teslim Süresi Sona Eriyor.Lütfen Zamanında Teslim Etmeye Özen Gösteriniz. ";
                    SmtpClient smtp = new SmtpClient();
                    smtp.Credentials = new System.Net.NetworkCredential("kutuphaneoto.ko@gmail.com", "administor");
                    smtp.Port = 587;
                    smtp.Host = "smtp.gmail.com";
                    smtp.EnableSsl = true;
                    smtp.Send(ePosta);
                    conn.Close();
                    MessageBox.Show(ad.ToString() + " Adlı Kullanıcıya Mail Gönderildi", " Tamamlandı", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch
            {

                MessageBox.Show("Bu Kullanıcını Mail Adresi Doğru Değil ", " Hata", MessageBoxButtons.RetryCancel, MessageBoxIcon.Error);
            }
        }

        private void contextMenuStrip1_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {

        }

        private void txtbsmyılı_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                e.Handled = true;
            }

            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            UyeFormu sa = new UyeFormu();
            sa.Show();
            Hide();
            

        }

        private void button1_Click_2(object sender, EventArgs e)
        {
            UyeFormu sa = new UyeFormu();
            sa.Show();
            Hide();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            timer3.Start();
        }

        private void timer3_Tick(object sender, EventArgs e)
        {
            if (isCollapsed3)
            {
                button2.Image = Resources.chevron_arrow_up;
                panel1.Height += 20;
                if (panel1.Size == panel1.MaximumSize)
                {
                    timer3.Stop();
                    isCollapsed3 = false;
                }
            }
            else
            {
                button2.Image = Resources.chevron_arrow_down;
                panel1.Height -= 20;
                if (panel1.Size == panel1.MinimumSize)
                {
                    timer3.Stop();
                    isCollapsed3 = true;
                }
            }
        }

        private void GrpboxBgn_Enter(object sender, EventArgs e)
        {

        }

        private void txtbsmyılı_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtbsmyılı_TextChanged_1(object sender, EventArgs e)
        {
            int u = txtbsmyılı.TextLength;
            lblbsmyl.Text = "4/" + txtbsmyılı.TextLength.ToString();

           
           
        }

        private void txtKitapAdi_TextChanged(object sender, EventArgs e)
        {
            int u = txtKitapAdi.TextLength;
            lblkitapadi.Text = "100/" + txtKitapAdi.TextLength.ToString();

         
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void txtQR_TextChanged(object sender, EventArgs e)
        {
            int u = txtQR.TextLength;
            lblQR.Text = "8/" + txtQR.TextLength.ToString();

           
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void cmbKitapTur_SelectedIndexChanged(object sender, EventArgs e)
        {
            int u = cmbKitapTur.SelectedIndex;
            lblktpTur.Text = "27/" + cmbKitapTur.SelectedIndex.ToString();
        }

        private void label12_Click(object sender, EventArgs e)
        {

        }

        private void txtYayinevi_TextChanged(object sender, EventArgs e)
        {
            int u = txtYayinevi.TextLength;
            lblyaynevi.Text = "50/" + txtYayinevi.TextLength.ToString();

        }

        private void txtKitapYazar_TextChanged(object sender, EventArgs e)
        {
            int u = txtKitapYazar.TextLength;
            lblkitpyazri.Text = "50/" + txtKitapYazar.TextLength.ToString();
        }

        private void PanelUst_Paint(object sender, PaintEventArgs e)
        {

        }

        private void PanelUst_MouseMove(object sender, MouseEventArgs e)
        {
            if (formTasiniyor)
            {
                Point p = PointToScreen(e.Location);
                Location = new Point(p.X - this.baslangicNoktasi.X, p.Y - this.baslangicNoktasi.Y);
            }

        }

        private void PanelUst_MouseUp(object sender, MouseEventArgs e)
        {
            formTasiniyor = false;
        }

        private void BtnIstatistikler_Click(object sender, EventArgs e)
        {
            Istatistikler();
        }
    }
}
