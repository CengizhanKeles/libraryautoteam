﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.Sql;
using System.Data.SqlClient;

namespace LibraryAutomation
{
    public partial class Sistem_Yoneticisi : Form
    {
        public Sistem_Yoneticisi()
        {
            InitializeComponent();
        }
        SqlConnection conn = new SqlConnection(@"Data source=.\SQLEXPRESS;Database=Kutuphane;Integrated Security=true;");
        //bool formTasiniyor = false;
        //Point baslangicNoktasi = new Point(0, 0);
        //private void PanelUst_MouseDown(object sender, MouseEventArgs e);
        private void kayitGetir()
        {

            listView1.GridLines = true;
            listView1.Columns.Clear();
            listView1.Items.Clear();
            conn.Open();
            if (Cmbsecim.Text == "Öğretmen")
            {

                listView1.Columns.Add("Adı", 192);
                listView1.Columns.Add("Soyadı", 192);
                listView1.Columns.Add("Kullanıcı Adı", 192);
                listView1.Columns.Add("Şifre", 192);
                listView1.Columns.Add("ID", 0);

                SqlCommand cmd = new SqlCommand("select * from UserLogin inner join Permission on UserLogin.PerID=Permission.ID where PerID=2 and Durum=0", conn);
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    ListViewItem item = new ListViewItem(dr["Name"].ToString());

                    item.SubItems.Add(dr["SurName"].ToString());
                    item.SubItems.Add(dr["UserName"].ToString());
                    item.SubItems.Add(dr["Password"].ToString());
                    item.SubItems.Add(dr["ID"].ToString());
                    listView1.Items.Add(item);
                }

                dr.Close();
                
               
            }
            else if (Cmbsecim.Text == "Öğrenci")
            {

                listView1.Columns.Add("Adı", 192);
                listView1.Columns.Add("Soyadı", 192);
                listView1.Columns.Add("Kullanıcı Adı", 192);
                listView1.Columns.Add("Şifre", 192);               
                listView1.Columns.Add("ID", 0);

                SqlCommand cmd = new SqlCommand("select * from UserLogin inner join Permission on UserLogin.PerID=Permission.ID where PerID=3 and Durum=0", conn);
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    ListViewItem item = new ListViewItem(dr["Name"].ToString());

                    item.SubItems.Add(dr["SurName"].ToString());
                    item.SubItems.Add(dr["UserName"].ToString());
                    item.SubItems.Add(dr["Password"].ToString());
                    item.SubItems.Add(dr["ID"].ToString());
                    listView1.Items.Add(item);
                }

                dr.Close();


            }
            else if (Cmbsecim.Text == "Sistem Yöneticisi")
            {


                listView1.Columns.Add("Adı", 192);
                listView1.Columns.Add("Soyadı", 192);
                listView1.Columns.Add("Kullanıcı Adı", 192);
                listView1.Columns.Add("Şifre", 192);
                listView1.Columns.Add("ID", 0);

                SqlCommand cmd = new SqlCommand("select * from UserLogin inner join Permission on UserLogin.PerID=Permission.ID where PerID=1 and Durum=0", conn);
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    ListViewItem item = new ListViewItem(dr["Name"].ToString());

                    item.SubItems.Add(dr["SurName"].ToString());
                    item.SubItems.Add(dr["UserName"].ToString());
                    item.SubItems.Add(dr["Password"].ToString());
                    item.SubItems.Add(dr["ID"].ToString());
                    listView1.Items.Add(item);
                }

                dr.Close();
            }
            conn.Close();
        }
        private void SecilenKontrol()
        {
            label3.Text = listView1.CheckedItems.Count.ToString();
            if (listView1.CheckedItems.Count > 0)
            {
                button1.Enabled = true;
            }
            else
            {
                button1.Enabled = false;
            }
        }
        private void Cmbsecim_SelectedIndexChanged(object sender, EventArgs e)
        {
            kayitGetir();
            groupBox2.Visible = true;
            button3.Visible = false;
             btnUserEkle.Text = "Ekle";
            txtUad.Clear();
            txtUkad.Clear();
            txtUsad.Clear();
            txtUsifre.Clear();

            cmbYetki.SelectedIndex = 0;

        }

        private void PictureCikis_Click(object sender, EventArgs e)
        {
            new Menu().YetkiliID = yetkiliid;
            new Menu().Yetkilendirme();
            new Menu().Show();
            Hide();
        }

        private void btnVeriekle_Click(object sender, EventArgs e)
        {

            //try
            //{
            //    if (btnVeriekle.Text == "Güncelle")
            //    {
                    
            //            conn.Open();
            //            string kayit = "update UserLogin set Name=@1,Surname=@2,Username=@3,Password=@4 where ID=@ID";
            //            SqlCommand komut = new SqlCommand(kayit, conn);
            //            komut.Parameters.AddWithValue("@1", txtAd.Text);
            //            komut.Parameters.AddWithValue("@2", txtSAd.Text);
            //            komut.Parameters.AddWithValue("@3", txtkad.Text);
            //            komut.Parameters.AddWithValue("@4", txtsifre.Text);
            //            komut.Parameters.AddWithValue("@ID", ID.ToString());
            //            komut.ExecuteNonQuery();
            //            conn.Close();
            //            btnUserEkle.Text = "Ekle";
            //            btnVeriekle.Text = "Ekle";
            //            listView1.Refresh();
            //            conn.Close();
            //            txtAd.Clear();
            //            txtSAd.Clear();
            //            txtkad.Clear();
                     
            //            cmbYetki.SelectedIndex = 0;
            //            kayitGetir();
            //            SecilenKontrol();
            //            MessageBox.Show("Öğretmen Bilgileri Güncellendi.");
                    
                  
            //    }
            //    else
            //    {
            //        DialogResult kontrol = new DialogResult();
            //        string verikontrolsorgusu = "";
            //        string parca = "";
            //        if (Cmbsecim.Text == "Öğretmen")
            //        {
            //            verikontrolsorgusu = "select count(*) from Teacher where ogretmenAd = @1 and ogretmenSoyad = @2 and ogretmenMail = @3 and ogretmenCinsiyet = @4 and Durum =@5";
            //            parca = "Teacher(ogretmenAd,ogretmenSoyad,ogretmenMail,ogretmenCinsiyet,Durum)";
            //        }
            //        else if (Cmbsecim.Text == "Öğrenci")
            //        {
            //            verikontrolsorgusu = "select count(*) from Student where ogrenciAd = @1 and ogrenciSoyad = @2 and ogrenciMail = @3 and ogrenciCinsiyet = @4 and Durum =@5";
            //            parca = "Student(ogrenciAd,ogrenciSoyad,ogrenciMail,ogrenciCinsiyet,Durum)";
            //        }

            //        if (txtAd.Text != "" && txtSAd.Text != "" && txtkad.Text != "")
            //        {
            //            SqlCommand verikontorol = new SqlCommand(verikontrolsorgusu, conn);
            //            verikontorol.Parameters.AddWithValue("@1", txtAd.Text);
            //            verikontorol.Parameters.AddWithValue("@2", txtSAd.Text);
            //            verikontorol.Parameters.AddWithValue("@3", txtkad.Text);
            //            verikontorol.Parameters.AddWithValue("@5", 0);

            //            conn.Open();
            //            int sonuc = (int)verikontorol.ExecuteScalar();
            //            conn.Close();
            //            if (sonuc > 0)
            //            {
            //                kontrol = MessageBox.Show("Bu veriden Zaten Var Yinede Eklemek İstiyormusun?", "Uyarı", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            //            }

            //            if (kontrol == DialogResult.Yes || sonuc == 0)
            //            {


            //                string komut = "insert into " + parca + "values(@1,@2,@3,@5)";
            //                SqlCommand cmd = new SqlCommand(komut, conn);
            //                cmd.Parameters.AddWithValue("@1", txtAd.Text);
            //                cmd.Parameters.AddWithValue("@2", txtSAd.Text);
            //                cmd.Parameters.AddWithValue("@3", txtkad.Text);
            //                cmd.Parameters.AddWithValue("@5", 0);
            //                conn.Open();
            //                cmd.ExecuteNonQuery();
            //                conn.Close();
            //                MessageBox.Show("Başarıyla Eklendi", "Bilgi", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //            }
            //        }
            //        else
            //        {
            //            MessageBox.Show("Yetki Seçmek Zorundasınız.", "Uyarı", MessageBoxButtons.OK, MessageBoxIcon.Warning);

            //        }
            //    }
            //}
            //catch(Exception ex )
            //{
            //    MessageBox.Show(ex.ToString());
            //    MessageBox.Show("Eklenemedi", "Bilgi", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //}

            //listView1.Refresh();
            //conn.Close();
            //txtAd.Clear();
            //txtSAd.Clear();
            //txtkad.Clear();
            
            //kayitGetir();
            //SecilenKontrol();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            try
            {
                
                for (int i = 0; i < listView1.CheckedItems.Count; i++)
                {
                    SqlCommand update = new SqlCommand("update UserLogin set Durum=1 where ID=@id",conn);             
                    update.Parameters.AddWithValue("@id", listView1.CheckedItems[i].SubItems[4].Text);

                    conn.Open();
                    update.ExecuteNonQuery();
                    conn.Close();
                }
                kayitGetir();
                SecilenKontrol();
                MessageBox.Show("Seçilenler Silindi");
            }
            catch (Exception ex)
            {
                MessageBox.Show(""+ex);
            }
           
            txtUad.Clear();
            txtUkad.Clear();
            txtUsad.Clear();
            txtUsifre.Clear();
            cmbYetki.SelectedIndex = 0;
            btnUserEkle.Text = "Ekle";
            button3.Visible = false;


        }

        private void ListView1_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            SecilenKontrol();
        }

        private void Sistem_Yoneticisi_Load(object sender, EventArgs e)
        {
            groupBox2.Visible = false;
            Cmbsecim.SelectedIndex = 0;
            cmbYetki.SelectedIndex = 0;
            listView1.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
            SqlCommand yetkigetir = new SqlCommand("select PermissionName from Permission ", conn);
            conn.Open();
            SqlDataReader dr = yetkigetir.ExecuteReader();
            while (dr.Read())
            {
                cmbYetki.Items.Add(dr["PermissionName"]);
            }
            conn.Close();

        }

        private void btnUserEkle_Click(object sender, EventArgs e)
        {
            

            try
            {
                if(btnUserEkle.Text=="Güncelle")
                {
                    conn.Open();
                    string kayit = "update UserLogin set Name=@1,Surname=@2,Username=@3,Password=@4 where ID=@ID";                  
                    SqlCommand komut = new SqlCommand(kayit, conn);               
                    komut.Parameters.AddWithValue("@1",txtUad.Text);
                    komut.Parameters.AddWithValue("@2", txtUsad.Text);
                    komut.Parameters.AddWithValue("@3", txtUkad.Text);
                    komut.Parameters.AddWithValue("@4", txtUsifre.Text);
                    komut.Parameters.AddWithValue("@ID", ID.ToString());
                    komut.ExecuteNonQuery();
                    conn.Close();

                    btnUserEkle.Text = "Ekle";                   
                    listView1.Refresh();
                    conn.Close();
                    txtUkad.Clear();
                    txtUsad.Clear();
                    txtUsifre.Clear();
                    txtUad.Clear();
                    cmbYetki.SelectedIndex = 0;
                    kayitGetir();
                    SecilenKontrol();
                    button3.Visible = false;
                    label4.Visible = true;
                    cmbYetki.Visible = true;
                   
                    MessageBox.Show("Kullanıcı Bilgileri Başarıyla Güncellendi","Başarılı",MessageBoxButtons.OK,MessageBoxIcon.Information);

                }
                else
                {
                    if (cmbYetki.Text == "" || cmbYetki.Text == null || cmbYetki.Text=="Seçiniz")
                    {
                        MessageBox.Show("Yetki Seçmek Zorundasınız.", "Uyarı", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                    else
                    { 
                        DialogResult kontrol = new DialogResult();
                        SqlCommand verikontorol = new SqlCommand("select count(*) from UserLogin Where Name = @1 and Surname =@2 and Username = @3 and Password = @4", conn);
                        verikontorol.Parameters.AddWithValue("@1", txtUad.Text);
                        verikontorol.Parameters.AddWithValue("@2", txtUsad.Text);
                        verikontorol.Parameters.AddWithValue("@3", txtUkad.Text);
                        verikontorol.Parameters.AddWithValue("@4", txtUsifre.Text);
                        conn.Open();
                        int sonuc = (int)verikontorol.ExecuteScalar();
                        conn.Close();
                        if (sonuc > 0)
                        {
                            kontrol = MessageBox.Show("Bu veriden Zaten Var Yinede Eklemek İstiyormusun?", "Uyarı", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                        }

                        if (kontrol == DialogResult.Yes || sonuc == 0)
                        {
                            SqlCommand yetkiidal = new SqlCommand("select ID from Permission where PermissionName = '" + cmbYetki.Text + "'", conn);
                            conn.Open();
                            int yetkiid = (int)yetkiidal.ExecuteScalar();
                            conn.Close();
                            SqlCommand cmd = new SqlCommand("insert into UserLogin(Name,Surname,Username,Password,PerID,Durum)values(@1,@2,@3,@4,@5,@6)", conn);
                            cmd.Parameters.AddWithValue("@1", txtUad.Text);
                            cmd.Parameters.AddWithValue("@2", txtUsad.Text);
                            cmd.Parameters.AddWithValue("@3", txtUkad.Text);
                            cmd.Parameters.AddWithValue("@4", txtUsifre.Text);
                            cmd.Parameters.AddWithValue("@5", yetkiid);
                            cmd.Parameters.AddWithValue("@6", 0);
                            conn.Open();
                            cmd.ExecuteNonQuery();
                            txtUad.Clear();
                            txtUkad.Clear();
                            txtUsad.Clear();
                            txtUsifre.Clear();
                            cmbYetki.SelectedIndex = 0;
                            MessageBox.Show("Kullanıcı Başarıyla Eklendi", "Başarılı", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("" + ex);
                // MessageBox.Show("Kayıt Eklenemedi", "İşlem Başarısız", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            listView1.Refresh();
            conn.Close();              
            kayitGetir();
            SecilenKontrol();
        }
        public int ID=0;
        private int yetkiliid;

        private void ListView1_SelectedIndexChanged(object sender, EventArgs e)
        {          
                if (listView1.SelectedItems.Count > 0)
            {
                ID = Convert.ToInt32(listView1.SelectedItems[0].SubItems[4].Text);
                btnUserEkle.Text = "Güncelle";              
                button3.Visible = true;
                label4.Visible = false;
                cmbYetki.Visible = false;
                txtUad.Text = listView1.SelectedItems[0].SubItems[0].Text;
                txtUsad.Text = listView1.SelectedItems[0].SubItems[1].Text;
                txtUkad.Text = listView1.SelectedItems[0].SubItems[2].Text;
                txtUsifre.Text = listView1.SelectedItems[0].SubItems[3].Text;
                
            
            }



        }

        private void ListView1_ItemSelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e)
        {

        }

        private void CmbYetki_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void Button2_Click(object sender, EventArgs e)
        {
        
        }

        private void Button3_Click(object sender, EventArgs e)
        {
            txtUad.Clear();
            txtUkad.Clear();
            txtUsad.Clear();
            txtUsifre.Clear();
            cmbYetki.SelectedIndex = 0;
            btnUserEkle.Text = "Ekle";
            button3.Visible = false;
            label4.Visible = true;
            cmbYetki.Visible = true;


        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            groupBox2.Visible = true;
        }
    }
}
