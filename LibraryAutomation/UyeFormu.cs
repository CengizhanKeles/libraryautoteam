﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.Sql;
using System.Data.SqlClient;

namespace LibraryAutomation
{
    public partial class UyeFormu : Form
    {
        public UyeFormu()
        {
            InitializeComponent();
        }
       
        public void refresh()
        {
            checkBox1.Checked = false;
            listView1.Items.Clear();
            conn.Open();
            SqlCommand shw = new SqlCommand("select Adı,Soyadı,Mail,Sinif,Brans,PermissionName,Durum,PerID,t1.ID from User_tbl t1 inner join Permission t2 on t1.PerID=t2.ID where Durum = 0", conn);
            SqlDataReader dr = shw.ExecuteReader();
            //  string kontrol = shw.ExecuteScalar().ToString();
            while (dr.Read())
            {
                ListViewItem item = new ListViewItem();
                item.SubItems.Add(dr[0].ToString());
                item.SubItems.Add(dr[1].ToString());
                item.SubItems.Add(dr[2].ToString());
                if (dr[3].ToString() == "")
                {
                    item.SubItems.Add(dr[4].ToString());

                }
                else
                {
                    item.SubItems.Add(dr[3].ToString());
                }

                item.SubItems.Add(dr[5].ToString());
                item.SubItems.Add(dr[8].ToString());
                listView1.Items.Add(item);

            }
            conn.Close();

        }
        SqlConnection conn = new SqlConnection(@"Data source=.\SQLEXPRESS;Database=Kutuphane;Integrated Security=true;");
        public int UserID = 0;
        private void UyeFormu_Load(object sender, EventArgs e)
        {
            

        }
        private void btnVeriekle_Click(object sender, EventArgs e)
        {

            if (btnVeriekle.Text == "Ekle")
            {

                conn.Open();
                SqlCommand komut = new SqlCommand(kayit, conn);
                komut.Parameters.AddWithValue("@1", txtAd.Text);
                komut.Parameters.AddWithValue("@2", txtSAd.Text);
                komut.Parameters.AddWithValue("@3", txtMail.Text);
                komut.Parameters.AddWithValue("@4", textBox3.Text);
                komut.Parameters.AddWithValue("@5", deger);
                komut.Parameters.AddWithValue("@6", 0);

                komut.ExecuteNonQuery();
                conn.Close();
                txtAd.Clear();
                txtSAd.Clear();
                txtMail.Clear();
                textBox3.Clear();
                refresh();

                MessageBox.Show("Üye Başarıyla Eklendi", "Başarılı", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                try
                {
                    if (update == "")
                    { 
                        if(checkBox1.Checked==true)
                        {
                            checkBox1.Checked = false;
                            checkBox1.Checked = true;
                            conn.Open();
                            SqlCommand komut = new SqlCommand(update, conn);
                            komut.Parameters.AddWithValue("@1", txtAd.Text);
                            komut.Parameters.AddWithValue("@2", txtSAd.Text);
                            komut.Parameters.AddWithValue("@3", txtMail.Text);
                            komut.Parameters.AddWithValue("@4", textBox3.Text);
                            komut.Parameters.AddWithValue("@5", deger);
                            komut.Parameters.AddWithValue("@6", ID);
                            komut.ExecuteNonQuery();
                            conn.Close();
                            txtAd.Clear();
                            txtSAd.Clear();
                            txtMail.Clear();
                            textBox3.Clear();
                            refresh();
                            btnVeriekle.Text = "Ekle";
                            button2.Visible = false;
                            MessageBox.Show("Üye Başarıyla Güncellendi", "Başarılı", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        else
                        {
                            checkBox1.Checked = false;
                            checkBox1.Checked = true;
                            conn.Open();
                            SqlCommand komut = new SqlCommand(update, conn);
                            komut.Parameters.AddWithValue("@1", txtAd.Text);
                            komut.Parameters.AddWithValue("@2", txtSAd.Text);
                            komut.Parameters.AddWithValue("@3", txtMail.Text);
                            komut.Parameters.AddWithValue("@4", textBox3.Text);
                            komut.Parameters.AddWithValue("@5", deger);
                            komut.Parameters.AddWithValue("@6", ID);
                            komut.ExecuteNonQuery();
                            conn.Close();
                            txtAd.Clear();
                            txtSAd.Clear();
                            txtMail.Clear();
                            textBox3.Clear();
                            refresh();
                            btnVeriekle.Text = "Ekle";
                            button2.Visible = false;
                            MessageBox.Show("Üye Başarıyla Güncellendi", "Başarılı", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    else
                    {
                        conn.Open();
                        SqlCommand komut = new SqlCommand(update, conn);
                        komut.Parameters.AddWithValue("@1", txtAd.Text);
                        komut.Parameters.AddWithValue("@2", txtSAd.Text);
                        komut.Parameters.AddWithValue("@3", txtMail.Text);
                        komut.Parameters.AddWithValue("@4", textBox3.Text);
                        komut.Parameters.AddWithValue("@5", deger);
                        komut.Parameters.AddWithValue("@6", ID);
                        komut.ExecuteNonQuery();
                        conn.Close();
                        txtAd.Clear();
                        txtSAd.Clear();
                        txtMail.Clear();
                        textBox3.Clear();
                        refresh();
                        btnVeriekle.Text = "Ekle";
                        button2.Visible = false;
                        MessageBox.Show("Üye Başarıyla Güncellendi", "Başarılı", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
                catch(Exception ex)
                {
                    MessageBox.Show(""+ex);
                }
            }
        }
        public int deger = 3;
        public string kayit = "insert into User_tbl (Adı,Soyadı,Mail,Sinif,PerID,Durum) VALUES (@1,@2,@3,@4,@5,@6)";
        public string update;
        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked == true)
            {
                label6.Text = "Branş";
                kayit = "insert into User_tbl (Adı,Soyadı,Mail,Brans,PerID,Durum) VALUES (@1,@2,@3,@4,@5,@6)";
                deger = 2;
                update = "update User_tbl set Adı=@1,Soyadı=@2,Mail=@3,Brans=@4,PerID=@5 where ID=@6";
            }
            else
            {
                label6.Text = "Sınıf";
                kayit = "insert into User_tbl (Adı,Soyadı,Mail,Sinif,PerID,Durum) VALUES (@1,@2,@3,@4,@5,@6)";
                deger = 3;
                update = "update User_tbl set Adı=@1,Soyadı=@2,Mail=@3,Sinif=@4,PerID=@5 where ID=@6";
            }
        }
        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
           
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            checkBox1.Checked = false;
            listView1.Items.Clear();
            conn.Open();
            SqlCommand shw = new SqlCommand("select Adı,Soyadı,Mail,Sinif,Brans,PermissionName,Durum,PerID,t1.ID from User_tbl t1 inner join Permission t2 on t1.PerID=t2.ID where UID ='"+textBox1.Text+"' and Durum = 0", conn);
            SqlDataReader dr = shw.ExecuteReader();
            //  string kontrol = shw.ExecuteScalar().ToString();
            while (dr.Read())
            {
                ListViewItem item = new ListViewItem();
                item.SubItems.Add(dr[0].ToString());
                item.SubItems.Add(dr[1].ToString());
                item.SubItems.Add(dr[2].ToString());
                if (dr[3].ToString() == "")
                {
                    item.SubItems.Add(dr[4].ToString());

                }
                else
                {
                    item.SubItems.Add(dr[3].ToString());
                }

                item.SubItems.Add(dr[5].ToString());
                item.SubItems.Add(dr[8].ToString());
                listView1.Items.Add(item);

            }
            conn.Close();
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
           
        }

        private void checkBox3_CheckedChanged(object sender, EventArgs e)
        {
         
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            DialogResult Result = MessageBox.Show("Seçili Kullanıcılar Silinecektir Onaylıyormsunuz ?", "Dikkat", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (Result == DialogResult.Yes)
            {
                try
                {
                    foreach (ListViewItem item in listView1.CheckedItems)
                    {
                        SqlCommand update = new SqlCommand("Update User_tbl set Durum=@1 where ID = '" + item.SubItems[6].Text + "'", conn);
                        conn.Open();
                        update.Parameters.AddWithValue("@1", 1);
                        update.ExecuteNonQuery();
                        conn.Close();
                    }
                    listView1.Items.Clear();
                    conn.Open();
                    SqlCommand shw = new SqlCommand("select Adı,Soyadı,Mail,Sinif,Brans,PermissionName,Durum,PerID,t1.ID from User_tbl t1 inner join Permission t2 on t1.PerID=t2.ID where Durum = 0", conn);
                    SqlDataReader dr = shw.ExecuteReader();
                    //  string kontrol = shw.ExecuteScalar().ToString();
                    while (dr.Read())
                    {
                        ListViewItem item = new ListViewItem();
                        item.SubItems.Add(dr[0].ToString());
                        item.SubItems.Add(dr[1].ToString());
                        item.SubItems.Add(dr[2].ToString());
                        if (dr[3].ToString() == "")
                        {
                            item.SubItems.Add(dr[4].ToString());

                        }
                        else
                        {
                            item.SubItems.Add(dr[3].ToString());
                        }

                        item.SubItems.Add(dr[5].ToString());
                        item.SubItems.Add(dr[8].ToString());
                        listView1.Items.Add(item);
                    }
                    conn.Close();
                    MessageBox.Show("Üye Başarıyla Silindi", "Başarılı", MessageBoxButtons.OK, MessageBoxIcon.Information);

                }
                catch (Exception ex)
                {
                    MessageBox.Show("Üye Silinemedi" + ex, "Hata", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void listView1_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            label3.Text = "Seçilen Üye Sayısı : " + listView1.CheckedItems.Count.ToString();
         
        }
        string brans = "";
        public int ID=0;
        private int yetkiliid;

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listView1.SelectedItems.Count > 0)
            {
                ID = Convert.ToInt32(listView1.SelectedItems[0].SubItems[6].Text.ToString());
                txtAd.Text = listView1.SelectedItems[0].SubItems[1].Text.ToString();
                txtSAd.Text = listView1.SelectedItems[0].SubItems[2].Text.ToString();
                txtMail.Text = listView1.SelectedItems[0].SubItems[3].Text.ToString();
                brans = listView1.SelectedItems[0].SubItems[5].Text.ToString();
                textBox3.Text= listView1.SelectedItems[0].SubItems[4].Text.ToString();

                if (brans == "Öğretmen")
                {
                    checkBox1.Checked = true;
                }
                else
                {
                    checkBox1.Checked = false;
                }
                btnVeriekle.Text = "Güncelle";
                button2.Visible = true;
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            txtAd.Clear();
            txtSAd.Clear();
            txtMail.Clear();
            textBox3.Clear();
            refresh();
            btnVeriekle.Text = "Ekle";
            button2.Visible = false;
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            new Menu().YetkiliID = yetkiliid;
            new Menu().Yetkilendirme();
            new Menu().Show();
            Hide();
        }
        bool formTasiniyor = false;
        Point baslangicNoktasi = new Point(0, 0);
        private void panel3_MouseDown(object sender, MouseEventArgs e)
        {
            formTasiniyor = true;
            baslangicNoktasi = new Point(e.X, e.Y);
        }

        private void panel3_MouseMove(object sender, MouseEventArgs e)
        {
            if (formTasiniyor)
            {
                Point p = PointToScreen(e.Location);
                Location = new Point(p.X - this.baslangicNoktasi.X, p.Y - this.baslangicNoktasi.Y);
            }
        }

        private void panel3_MouseUp(object sender, MouseEventArgs e)
        {
            formTasiniyor = false;
        }
    }
}
