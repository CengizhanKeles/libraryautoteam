﻿namespace LibraryAutomation
{
    partial class Iletisim
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Iletisim));
            this.PanelUst = new System.Windows.Forms.Panel();
            this.LblU = new System.Windows.Forms.Label();
            this.pictureKucult = new System.Windows.Forms.PictureBox();
            this.pictureCikis = new System.Windows.Forms.PictureBox();
            this.pcInsta = new System.Windows.Forms.PictureBox();
            this.pcFacebook = new System.Windows.Forms.PictureBox();
            this.pcTwitter = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.label3 = new System.Windows.Forms.Label();
            this.PanelUst.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureKucult)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureCikis)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcInsta)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcFacebook)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcTwitter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // PanelUst
            // 
            this.PanelUst.BackColor = System.Drawing.Color.SteelBlue;
            this.PanelUst.Controls.Add(this.LblU);
            this.PanelUst.Controls.Add(this.pictureKucult);
            this.PanelUst.Controls.Add(this.pictureCikis);
            this.PanelUst.Location = new System.Drawing.Point(0, 0);
            this.PanelUst.Name = "PanelUst";
            this.PanelUst.Size = new System.Drawing.Size(374, 36);
            this.PanelUst.TabIndex = 4;
            this.PanelUst.MouseDown += new System.Windows.Forms.MouseEventHandler(this.PanelUst_MouseDown);
            this.PanelUst.MouseMove += new System.Windows.Forms.MouseEventHandler(this.PanelUst_MouseMove);
            this.PanelUst.MouseUp += new System.Windows.Forms.MouseEventHandler(this.PanelUst_MouseUp);
            // 
            // LblU
            // 
            this.LblU.AutoSize = true;
            this.LblU.Font = new System.Drawing.Font("Comic Sans MS", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblU.ForeColor = System.Drawing.SystemColors.Control;
            this.LblU.Location = new System.Drawing.Point(3, 2);
            this.LblU.Name = "LblU";
            this.LblU.Size = new System.Drawing.Size(188, 30);
            this.LblU.TabIndex = 17;
            this.LblU.Text = "LibraU ~ İletişim";
            // 
            // pictureKucult
            // 
            this.pictureKucult.Image = ((System.Drawing.Image)(resources.GetObject("pictureKucult.Image")));
            this.pictureKucult.Location = new System.Drawing.Point(286, 1);
            this.pictureKucult.Name = "pictureKucult";
            this.pictureKucult.Size = new System.Drawing.Size(38, 34);
            this.pictureKucult.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureKucult.TabIndex = 29;
            this.pictureKucult.TabStop = false;
            // 
            // pictureCikis
            // 
            this.pictureCikis.Image = ((System.Drawing.Image)(resources.GetObject("pictureCikis.Image")));
            this.pictureCikis.Location = new System.Drawing.Point(330, 0);
            this.pictureCikis.Name = "pictureCikis";
            this.pictureCikis.Size = new System.Drawing.Size(41, 34);
            this.pictureCikis.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureCikis.TabIndex = 28;
            this.pictureCikis.TabStop = false;
            this.pictureCikis.Click += new System.EventHandler(this.PictureCikis_Click);
            // 
            // pcInsta
            // 
            this.pcInsta.Image = ((System.Drawing.Image)(resources.GetObject("pcInsta.Image")));
            this.pcInsta.Location = new System.Drawing.Point(218, 19);
            this.pcInsta.Name = "pcInsta";
            this.pcInsta.Size = new System.Drawing.Size(34, 32);
            this.pcInsta.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pcInsta.TabIndex = 9;
            this.pcInsta.TabStop = false;
            this.pcInsta.Click += new System.EventHandler(this.PictureBox3_Click);
            // 
            // pcFacebook
            // 
            this.pcFacebook.Image = ((System.Drawing.Image)(resources.GetObject("pcFacebook.Image")));
            this.pcFacebook.Location = new System.Drawing.Point(258, 19);
            this.pcFacebook.Name = "pcFacebook";
            this.pcFacebook.Size = new System.Drawing.Size(34, 32);
            this.pcFacebook.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pcFacebook.TabIndex = 8;
            this.pcFacebook.TabStop = false;
            this.pcFacebook.Click += new System.EventHandler(this.PcFacebook_Click);
            // 
            // pcTwitter
            // 
            this.pcTwitter.Image = ((System.Drawing.Image)(resources.GetObject("pcTwitter.Image")));
            this.pcTwitter.Location = new System.Drawing.Point(298, 19);
            this.pcTwitter.Name = "pcTwitter";
            this.pcTwitter.Size = new System.Drawing.Size(34, 32);
            this.pcTwitter.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pcTwitter.TabIndex = 7;
            this.pcTwitter.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(178, 19);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(34, 32);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 13;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(6, 10);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(70, 70);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 9;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.PictureBox3_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.linkLabel1);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.pictureBox2);
            this.groupBox2.Controls.Add(this.pcInsta);
            this.groupBox2.Controls.Add(this.pcFacebook);
            this.groupBox2.Controls.Add(this.pcTwitter);
            this.groupBox2.Controls.Add(this.pictureBox1);
            this.groupBox2.Location = new System.Drawing.Point(8, 54);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(354, 223);
            this.groupBox2.TabIndex = 12;
            this.groupBox2.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label1.Location = new System.Drawing.Point(6, 99);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(222, 19);
            this.label1.TabIndex = 65;
            this.label1.Text = "Mail : Mehmetrifat@gmail.com";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label4.Location = new System.Drawing.Point(6, 137);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(101, 19);
            this.label4.TabIndex = 64;
            this.label4.Text = "Web Sitemiz :";
            // 
            // linkLabel1
            // 
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.Location = new System.Drawing.Point(113, 143);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(163, 13);
            this.linkLabel1.TabIndex = 63;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "Mehmetrifatevyapteml.meb.k12.tr";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label3.Location = new System.Drawing.Point(6, 118);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(189, 19);
            this.label3.TabIndex = 62;
            this.label3.Text = "Bizi Arayın : 0212 289 6584";
            // 
            // Iletisim
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(365, 278);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.PanelUst);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Iletisim";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Iletisim";
            this.PanelUst.ResumeLayout(false);
            this.PanelUst.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureKucult)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureCikis)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcInsta)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcFacebook)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcTwitter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel PanelUst;
        private System.Windows.Forms.Label LblU;
        private System.Windows.Forms.PictureBox pictureKucult;
        private System.Windows.Forms.PictureBox pictureCikis;
        private System.Windows.Forms.PictureBox pcTwitter;
        private System.Windows.Forms.PictureBox pcFacebook;
        private System.Windows.Forms.PictureBox pcInsta;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.LinkLabel linkLabel1;
        private System.Windows.Forms.Label label3;
    }
}