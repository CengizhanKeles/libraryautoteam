﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Text;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LibraryAutomation
{
    public partial class EmanetVer : Form
    {
        public EmanetVer()
        {
            InitializeComponent();
        }
        public int yetkiliid = 0;
        private void PictureBox1_Click(object sender, EventArgs e)
        {

            Menu menu = new Menu();
            menu.YetkiliID = yetkiliid;
            menu.Yetkilendirme();
            menu.Show();
            Hide();

        }
        SqlConnection conn = new SqlConnection(@"Data source=.\SQLEXPRESS;Database=Kutuphane;Integrated Security=true;");

        private void PictureBox2_Click(object sender, EventArgs e)
        {
            this.WindowState = System.Windows.Forms.FormWindowState.Minimized;
        }

        private void EmanetVerAl_Load(object sender, EventArgs e)
        {

            lvKitap.View = View.Details;
            lvKitap.GridLines = true;
            lvKitap.FullRowSelect = true;
            lvKitap.Columns.Add("Adı Adı", 170);
            lvKitap.Columns.Add("Kitap Yazarı", 125);
            lvKitap.Columns.Add("Kitap Türü", 150);


            lvKitap.AutoResizeColumns(ColumnHeaderAutoResizeStyle.None);


            lvKullanici.View = View.Details;
            lvKullanici.GridLines = true;
            lvKullanici.FullRowSelect = true;
            lvKullanici.Columns.Add("Ad", 180);
            lvKullanici.Columns.Add("Soyadı", 180);
            lvKullanici.Columns.Add("Mail", 180);
            lvKullanici.Columns.Add("Yetki", 180);
            lvKullanici.Columns.Add("Id", 0);
            lvKullanici.AutoResizeColumns(ColumnHeaderAutoResizeStyle.None);



            // SqlCommand user = new SqlCommand("select * from User_tbl inner join Permission on Permission.ID=User_tbl.PerID",conn);
        }

        private void LvKitap_ColumnWidthChanged(object sender, ColumnWidthChangedEventArgs e)
        {
            if (lvKitap.Columns[1].Width > 0)
            {
                lvKitap.Columns[1].Width = 0;
            }
        }



        int kontrol = 0;
        int kitapSayisi = 0;
        int kullanicid = 0;
        int kitapid = 0;
        int kitapkon = 0;
        DateTime dateForButton = DateTime.Now.AddDays(+7);
        DateTime dateForButton1 = DateTime.Now;
        DateTime alis;
        TimeSpan Sonuc;
        private void BtnEmanetVer_Click(object sender, EventArgs e)
        {
            if (lvKitap.SelectedItems.Count > 0)
            {


                DialogResult result = MessageBox.Show("Seçili Kitap Emanet Verilecektir Onaylıyor musunuz ?", "Dikkat", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (result == DialogResult.Yes)
                {
                    //try
                    //{

                    if (conn.State == ConnectionState.Closed)
                    {
                        conn.Open();
                    }
                    SqlCommand cmd = new SqlCommand("select * from Brow where UserID = '" + lvKullanici.SelectedItems[0].SubItems[4].Text + "'", conn);
                    SqlDataReader dr2 = cmd.ExecuteReader();
                    while (dr2.Read())
                    {
                        kitapSayisi++;
                    }
                    dr2.Close();
                    SqlCommand komut = new SqlCommand("select BookID from Brow where BookID = '" + lvKitap.SelectedItems[0].SubItems[1].Text + "'", conn);
                    SqlDataReader dr = komut.ExecuteReader();
                    if (!dr.Read())
                    {
                        dr.Close();
                        if (conn.State == ConnectionState.Closed)
                        {
                            conn.Open();
                        }
                        SqlCommand cmd6 = new SqlCommand("select count(*) from Brow where BookID='" + lvKitap.SelectedItems[0].SubItems[1].Text + "'", conn);
                        int sonuc = (int)cmd6.ExecuteScalar();
                        conn.Close();
                        if (sonuc == 0)
                        {
                            if (kitapSayisi < 3)
                            {
                                if (conn.State == ConnectionState.Closed)
                                {
                                    conn.Open();
                                }

                                string query1 = "insert into Brow(UserID,BookID,Date,Takeit,Durum)values(@1,@2,@3,@4,@5)";
                                SqlCommand cmd1 = new SqlCommand(query1, conn);
                                cmd1.Parameters.AddWithValue("@1", Convert.ToInt32(lvKullanici.SelectedItems[0].SubItems[4].Text));
                                cmd1.Parameters.AddWithValue("@2", Convert.ToInt32(lvKitap.SelectedItems[0].SubItems[1].Text));
                                cmd1.Parameters.AddWithValue("@3", DateTime.Parse(dateForButton1.ToString()));
                                cmd1.Parameters.AddWithValue("@4", DateTime.Parse(dateForButton.ToString()));
                                cmd1.Parameters.AddWithValue("@5", 0);
                                cmd1.ExecuteNonQuery();
                                conn.Close();
                                DialogResult oh = MessageBox.Show("Emanet Verildi", "Bilgi", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                if (oh == DialogResult.OK)
                                {
                                    kitapkon = 0;
                                    kontrol = 0;
                                }

                            }
                            else
                            {
                                MessageBox.Show("Bu kullanıcın Şuanda Zaten 3 Emaneti Var lütfen Birini Alıp Tekrar Deneyiniz", "Uyarı", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                kontrol = 2;
                            }
                        }
                        else
                        {
                            MessageBox.Show("Bu Kitap Zaten Bu Kullanıcıda Mevcut ", " Hata", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        conn.Close();
                    }
                    else
                    {
                        MessageBox.Show("Bu Kitap Şuanda Emanette Bulunuyor ", " Bilgi", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    dr.Close();

                    //}
                    //catch (Exception ex)
                    //{

                    //    MessageBox.Show("Hata" + ex);
                    //}

                }
                else if (result == DialogResult.No)
                {
                    MessageBox.Show("Emanet Verilmedi", "Bilgi", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    kontrol = 2;
                }
                kitapSayisi = 0;
            }
        }
        private void araRFID()
        {


            lvKullanici.Items.Clear();

            SqlCommand cmd2 = new SqlCommand("select * from User_tbl  inner join Permission on Permission.ID=User_tbl.PerID where UID like '%" + txtKullanici.Text + "%' and durum=0", conn);
            conn.Open();
            SqlDataReader dr2 = cmd2.ExecuteReader();
            while (dr2.Read())
            {
                ListViewItem item2 = new ListViewItem(dr2[2].ToString());
                item2.SubItems.Add(dr2[3].ToString());
                item2.SubItems.Add(dr2[4].ToString());
                item2.SubItems.Add(dr2[9].ToString());

                item2.SubItems.Add(dr2[0].ToString());

                lvKullanici.Items.Add(item2);
            }
            dr2.Close();
            conn.Close();
        }
        private void araQR()
        {
            lvKitap.Items.Clear();

            SqlCommand doldur = new SqlCommand("select * from Book where QR = '" + txtQR.Text + "'and Durum=0", conn);
            conn.Open();
            SqlDataReader dr = doldur.ExecuteReader();
            while (dr.Read())
            {
                ListViewItem item = new ListViewItem(dr["QR"].ToString());
                item.SubItems.Add(dr["ID"].ToString());
                item.SubItems.Add(dr["BookName"].ToString());
                item.SubItems.Add(dr["BookAuthor"].ToString());
                item.SubItems.Add(dr["BookType"].ToString());
                lvKitap.Items.Add(item);
            }
            dr.Close();
            conn.Close();

        }



        private void txtKullanici_TextChanged_1(object sender, EventArgs e)
        {
            araRFID();
        }

        private void txtQR_TextChanged_1(object sender, EventArgs e)
        {
            araQR();
        }
        public int ct2 = 0;
        private void button3_Click(object sender, EventArgs e)
        {
            if (ct2 == 0)
            {
                groupBox5.Visible = true;
                ct2 = 1;
                button3.Text = "Kodla Ara";



            }
            else
            {
                groupBox5.Visible = false;
                ct2 = 0;
                button3.Text = "Detaylı Ara";
                textBox1.Text = "";

                cmbKitapTur.SelectedIndex = 0;


            }
        }

        private void cmbKitapTur_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbKitapTur.SelectedIndex == 0)
            {
                lvKitap.Items.Clear();
            }
            else if (cmbKitapTur.SelectedIndex == 1)
            {
                lvKitap.Items.Clear();

                SqlCommand doldur = new SqlCommand("select * from Book where Durum=0", conn);
                conn.Open();
                SqlDataReader dr = doldur.ExecuteReader();
                while (dr.Read())
                {
                    ListViewItem item = new ListViewItem(dr["QR"].ToString());
                    item.SubItems.Add(dr["ID"].ToString());
                    item.SubItems.Add(dr["BookName"].ToString());
                    item.SubItems.Add(dr["BookAuthor"].ToString());
                    item.SubItems.Add(dr["BookType"].ToString());
                    lvKitap.Items.Add(item);
                }
                dr.Close();
                conn.Close();
            }
            else
            {
                lvKitap.Items.Clear();

                SqlCommand doldur = new SqlCommand("select * from Book where BookType = '" + cmbKitapTur + "'and Durum=0", conn);
                conn.Open();
                SqlDataReader dr = doldur.ExecuteReader();
                while (dr.Read())
                {
                    ListViewItem item = new ListViewItem(dr["QR"].ToString());
                    item.SubItems.Add(dr["ID"].ToString());
                    item.SubItems.Add(dr["BookName"].ToString());
                    item.SubItems.Add(dr["BookAuthor"].ToString());
                    item.SubItems.Add(dr["BookType"].ToString());
                    lvKitap.Items.Add(item);
                }
                dr.Close();
                conn.Close();
            }
        }

        private void txtQR_TextChanged(object sender, EventArgs e)
        {
            araQR();
        }

        private void groupBox5_Enter(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            lvKitap.Items.Clear();

            SqlCommand doldur = new SqlCommand("select * from Book where BookName = '%" + textBox1.Text + "%'and Durum=0", conn);
            conn.Open();
            SqlDataReader dr = doldur.ExecuteReader();
            while (dr.Read())
            {
                ListViewItem item = new ListViewItem(dr["QR"].ToString());
                item.SubItems.Add(dr["ID"].ToString());
                item.SubItems.Add(dr["BookName"].ToString());
                item.SubItems.Add(dr["BookAuthor"].ToString());
                item.SubItems.Add(dr["BookType"].ToString());
                lvKitap.Items.Add(item);
            }
            dr.Close();
            conn.Close();
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void panel1_MouseMove(object sender, MouseEventArgs e)
        {
            if (formTasiniyor)
            {
                Point p = PointToScreen(e.Location);
                Location = new Point(p.X - this.baslangicNoktasi.X, p.Y - this.baslangicNoktasi.Y);
            }

        }
        bool formTasiniyor = false;
        Point baslangicNoktasi = new Point(0, 0);

        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            formTasiniyor = true;
            baslangicNoktasi = new Point(e.X, e.Y);
        }

        private void panel1_MouseMove_1(object sender, MouseEventArgs e)
        {

            if (formTasiniyor)
            {
                Point p = PointToScreen(e.Location);
                Location = new Point(p.X - this.baslangicNoktasi.X, p.Y - this.baslangicNoktasi.Y);
            }

        }

        private void panel1_MouseUp(object sender, MouseEventArgs e)
        {
            formTasiniyor = false;
        }
    }
}
