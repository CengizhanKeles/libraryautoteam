﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.Sql;
using System.Data.SqlClient;
namespace LibraryAutomation
{
    public partial class EmanetAl : Form
    {
        public EmanetAl()
        {
            InitializeComponent();
        }
        SqlConnection conn = new SqlConnection(@"Data source=.\SQLEXPRESS;Database=Kutuphane;Integrated Security=true;");
        int ct = 0;
        public void refresh()
        {
           
            conn.Open();
            string sorgu = "select t1.ID,t2.Adı, t3.BookName,t1.Date,t1.Takeit,t1.Durum from Brow t1 inner join User_tbl t2 on t1.UserID=t2.ID inner join Book t3 on t1.BookID = t3.ID where t1.Durum=0";
            SqlCommand cmd = new SqlCommand(sorgu, conn);
            SqlDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                ListViewItem item = new ListViewItem(dr["BookName"].ToString());
                item.SubItems.Add(dr["ID"].ToString());
                item.SubItems.Add(dr["Adı"].ToString());
                item.SubItems.Add(dr["Date"].ToString());
                item.SubItems.Add(dr["Takeit"].ToString());
                lvEmanetAl.Items.Add(item);
            }
            conn.Close();

        }
        private void BtnEmanetAl_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult al = MessageBox.Show("Emaneti Teslim Almayı Onaylıyormsunuz", "Teslim Alınacak ?", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                if (al == DialogResult.Yes)
                {
                    SqlCommand cmd = new SqlCommand("Update Brow set Durum=@Durum where ID=@ID", conn);
                    conn.Open();
                    cmd.Parameters.AddWithValue("@Durum", 1);
                    cmd.Parameters.AddWithValue("@ID", ID);
                    cmd.ExecuteNonQuery();
                    conn.Close();
                    refresh();
                }
            }
            catch (Exception ex) { MessageBox.Show("" + ex); }

        }


        private void EmanetAl_Load(object sender, EventArgs e)
        {
            lvEmanetAl.Items.Clear();
            if (ct == 0)
            {
                lvEmanetAl.View = View.Details;
                lvEmanetAl.GridLines = true;
                lvEmanetAl.FullRowSelect = true;
                lvEmanetAl.Columns.Add("Kitabı Alan Kişinin Adı", 150);
                lvEmanetAl.Columns.Add("Verildiği Tarih", 150);
                lvEmanetAl.Columns.Add("Geri Alınacağı Tarih", 150);
                lvEmanetAl.AutoResizeColumns(ColumnHeaderAutoResizeStyle.None);
                ct = 1;
            }
        

        }

        private void LvEmanetAl_ColumnWidthChanged(object sender, ColumnWidthChangedEventArgs e)
        {
            if (lvEmanetAl.Columns[1].Width > 0)
            {
                lvEmanetAl.Columns[1].Width = 0;
            }
        }
        public int ID = 0;
        private void lvEmanetAl_SelectedIndexChanged(object sender, EventArgs e)
        {
            ID = Convert.ToInt32(lvEmanetAl.SelectedItems[0].SubItems[1].Text);

        }

        private void araKullanici()
        {
            lvEmanetAl.Items.Clear();
            if (txtKullanici.Text.Length > 0)
            {

                SqlCommand cmd2 = new SqlCommand("select ogrenciAd,ogrenciSoyad,ogrenciMail,ID from User_tbl where UID like '%" + txtKullanici.Text + "%' and durum=0", conn);
                conn.Open();
                SqlDataReader dr2 = cmd2.ExecuteReader();
                while (dr2.Read())
                {
                    ListViewItem item2 = new ListViewItem(dr2[0].ToString());
                    item2.SubItems.Add(dr2[1].ToString());
                    item2.SubItems.Add(dr2[2].ToString());
                    item2.SubItems.Add(dr2[3].ToString());

                    lvEmanetAl.Items.Add(item2);
                }
                dr2.Close();
                conn.Close();
            }

        }
        private void araKitap()
        {
            lvEmanetAl.Items.Clear();

            SqlCommand doldur = new SqlCommand("select t1.ID,t2.Adı, t3.BookName,t1.Date,t1.Takeit,t1.Durum from Brow t1 inner join User_tbl t2 on t1.UserID=t2.ID inner join Book t3 on t1.BookID = t3.ID where QR = '" + txtQR.Text + "'and durum=0", conn);
            conn.Open();
            SqlDataReader dr = doldur.ExecuteReader();
            while (dr.Read())
            {
                ListViewItem item = new ListViewItem(dr["BookName"].ToString());
                item.SubItems.Add(dr["ID"].ToString());
                item.SubItems.Add(dr["Adı"].ToString());
                item.SubItems.Add(dr["Date"].ToString());
                item.SubItems.Add(dr["Takeit"].ToString());
                lvEmanetAl.Items.Add(item);
            }
            dr.Close();
            conn.Close();

        }
        public void detaylıara()
        {
            lvEmanetAl.Items.Clear();

            SqlCommand doldur = new SqlCommand("select t1.ID,t2.Adı, t3.BookName,t1.Date,t1.Takeit,t1.Durum from Brow t1 inner join User_tbl t2 on t1.UserID=t2.ID inner join Book t3 on t1.BookID = t3.ID where BookType = '" + cmbKitapTur.Text + "'and durum=0", conn);
            conn.Open();
            SqlDataReader dr = doldur.ExecuteReader();
            while (dr.Read())
            {
                ListViewItem item = new ListViewItem(dr["BookName"].ToString());
                item.SubItems.Add(dr["ID"].ToString());
                item.SubItems.Add(dr["Adı"].ToString());
                item.SubItems.Add(dr["Date"].ToString());
                item.SubItems.Add(dr["Takeit"].ToString());
                lvEmanetAl.Items.Add(item);
            }
            dr.Close();
            conn.Close();
        }

        private void txtKullanici_TextChanged(object sender, EventArgs e)
        {
            araKullanici();
        }

        private void txtMail_TextChanged(object sender, EventArgs e)
        {
            araKullanici();
        }

        private void txtQR_TextChanged(object sender, EventArgs e)
        {
            araKitap();
        }

        private void txtKitapAdi_TextChanged(object sender, EventArgs e)
        {
            araKitap();
        }

        private void txtYazar_TextChanged(object sender, EventArgs e)
        {
            araKitap();
        }

        private void cmbKitapTur_SelectedIndexChanged(object sender, EventArgs e)
        {
            araKitap();
        }
        public int ct2 = 0;
        private int yetkiliid;

        private void button3_Click(object sender, EventArgs e)
        {
            if (ct2 == 0)
            {
                groupBox2.Visible = true;
                ct2 = 1;
                button3.Text = "Kodla Ara";



            }
            else
            {
                groupBox2.Visible = false;
                ct2 = 0;
                button3.Text = "Detaylı Ara";
                textBox1.Text = "";
                
                cmbKitapTur.SelectedIndex = 0;


            }

        }

        private void cmbKitapTur_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            if(cmbKitapTur.SelectedIndex==0)
            {
                lvEmanetAl.Items.Clear();
            }
           else if(cmbKitapTur.SelectedIndex==1)
            {
                refresh();
            }
            else
            {
                lvEmanetAl.Items.Clear();

                SqlCommand doldur = new SqlCommand("select t1.ID,t2.Adı, t3.BookName,t1.Date,t1.Takeit,t1.Durum from Brow t1 inner join User_tbl t2 on t1.UserID=t2.ID inner join Book t3 on t1.BookID = t3.ID where t3.BookType = '" + cmbKitapTur.Text + "'and t1.durum=0", conn);
                conn.Open();
                SqlDataReader dr = doldur.ExecuteReader();
                while (dr.Read())
                {
                    ListViewItem item = new ListViewItem(dr["BookName"].ToString());
                    item.SubItems.Add(dr["ID"].ToString());
                    item.SubItems.Add(dr["Adı"].ToString());
                    item.SubItems.Add(dr["Date"].ToString());
                    item.SubItems.Add(dr["Takeit"].ToString());
                    lvEmanetAl.Items.Add(item);
                }
                dr.Close();
                conn.Close();
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if(textBox1.Text=="")
                {
                    lvEmanetAl.Items.Clear();
                }
                lvEmanetAl.Items.Clear();

                SqlCommand doldur = new SqlCommand("select t1.ID,t2.Adı, t3.BookName,t1.Date,t1.Takeit,t1.Durum from Brow t1 inner join User_tbl t2 on t1.UserID=t2.ID inner join Book t3 on t1.BookID = t3.ID where t3.BookName like '%" + textBox1.Text + "%'and t1.durum=0", conn);
                conn.Open();
                SqlDataReader dr = doldur.ExecuteReader();
                while (dr.Read())
                {
                    ListViewItem item = new ListViewItem(dr["BookName"].ToString());
                    item.SubItems.Add(dr["ID"].ToString());
                    item.SubItems.Add(dr["Adı"].ToString());
                    item.SubItems.Add(dr["Date"].ToString());
                    item.SubItems.Add(dr["Takeit"].ToString());
                    lvEmanetAl.Items.Add(item);
                }
                dr.Close();
                conn.Close();
                if (textBox1.Text == "")
                {
                    lvEmanetAl.Items.Clear();
                }
            }
            catch (Exception ex) 
            { MessageBox.Show(""+ex); }
        }
        private void pictureBox1_Click(object sender, EventArgs e)
        {
            Menu menu = new Menu();
            menu.YetkiliID = yetkiliid;
            menu.Yetkilendirme();
            menu.Show();
            this.Hide();
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            this.WindowState = System.Windows.Forms.FormWindowState.Minimized;
        }
        bool formTasiniyor = false;
        Point baslangicNoktasi = new Point(0, 0);

        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            formTasiniyor = true;
            baslangicNoktasi = new Point(e.X, e.Y);
        }

        private void panel1_MouseMove(object sender, MouseEventArgs e)
        {
            if (formTasiniyor)
            {
                Point p = PointToScreen(e.Location);
                Location = new Point(p.X - this.baslangicNoktasi.X, p.Y - this.baslangicNoktasi.Y);
            }
        }

        private void panel1_MouseUp(object sender, MouseEventArgs e)
        {
            formTasiniyor = false;
        }
    }
}
