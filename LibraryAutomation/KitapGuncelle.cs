﻿using MessagingToolkit.QRCode.Codec;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LibraryAutomation
{
    public partial class KitapGuncelle : Form
    {
        public KitapGuncelle()
        {
            InitializeComponent();
        }
        SqlConnection conn = new SqlConnection(@"Data source=.\SQLEXPRESS;Database=Kutuphane;Integrated Security=true;");
        private void KitapGuncelle_Load(object sender, EventArgs e)
        {
            lvKitap.View = View.Details;
            lvKitap.GridLines = true;
            lvKitap.FullRowSelect = true;
            lvKitap.Columns.Add("QR", 125);
            lvKitap.Columns.Add("Kitap Adı", 125);
            lvKitap.Columns.Add("Kitap Yazarı", 125);
            lvKitap.Columns.Add("Kitap Türü", 125);
            lvKitap.Columns.Add("ID", 1);
            cmbKitapTur.Text = "Seçiniz";

            pcbQR.Visible = false;



            /*SqlCommand cmd = new SqlCommand("select * from Book where durum=0", conn);
            conn.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                ListViewItem item = new ListViewItem(dr["QR"].ToString());
                item.SubItems.Add(dr["BookName"].ToString());
                item.SubItems.Add(dr["BookAuthor"].ToString());
                item.SubItems.Add(dr["BookType"].ToString());
                item.SubItems.Add(dr["ID"].ToString());

                lvKitap.Items.Add(item);
            }
            dr.Close();
            conn.Close();*/
        }
        private Image KareKodOlustur(string giris, int kkDuzey)
        {
            var deger = giris;
            MessagingToolkit.QRCode.Codec.QRCodeEncoder qe = new MessagingToolkit.QRCode.Codec.QRCodeEncoder();
            qe.QRCodeEncodeMode = QRCodeEncoder.ENCODE_MODE.BYTE;
            qe.QRCodeErrorCorrect = QRCodeEncoder.ERROR_CORRECTION.L;
            qe.QRCodeVersion = kkDuzey;
            System.Drawing.Bitmap bm = qe.Encode(deger);
            return bm;
        }
        private void LvKitap_ColumnWidthChanged(object sender, ColumnWidthChangedEventArgs e)
        {

            if (lvKitap.Columns[1].Width > 0)
            {
                lvKitap.Columns[1].Width = 0;
            }
        }
        public int ID = 0;
        private void LvKitap_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (lvKitap.SelectedItems.Count > 0)
                {
                    conn.Open();
                    ID = Convert.ToInt32(lvKitap.SelectedItems[0].SubItems[4].Text);
                    SqlCommand cmd = new SqlCommand("select * from book where ID=@ID", conn);
                    cmd.Parameters.AddWithValue("@ID", ID);

                    SqlDataReader dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        txtKitapAdi.Text = dr["BookName"].ToString();
                        txtKitapYazar.Text = dr["BookAuthor"].ToString();
                        txtYayinevi.Text = dr["Publisher"].ToString();
                        txtQR.Text = dr["QR"].ToString();
                        txtBasimYili.Text = dr["YearPrint"].ToString();
                        cmbKitapTur.Text = dr["BookType"].ToString();
                        txtSayfaSayisi.Text = dr["PageNumber"].ToString();
                        rtxtOzet.Text = dr["BookSummary"].ToString();
                        txtdrm.Text = dr["Durum"].ToString();
                    }
                    conn.Close();
                }
                try
                {
                    pcbQR.Image = KareKodOlustur(txtQR.Text, 4);
                    pcbQR.Visible = true;
                }
                catch
                {
                    MessageBox.Show("URL adresiniz için Kare kod oluşturulamadı.", "Hata !", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (Exception ex)
            { MessageBox.Show("" + ex); }
        }
        private void BtnTemizle_Click(object sender, EventArgs e)
        {
            txtQR.Clear();
            txtKitapAdi.Clear();
            txtKitapYazar.Clear();
            txtSayfaSayisi.Clear();
            txtYayinevi.Clear();
            rtxtOzet.Clear();
            pcbQR.Visible = false;
            txtBasimYili.Clear();
            cmbKitapTur.Text = "Seçiniz";
            textBox2.Clear();
            ID = -122;
        }
        private void BtnGuncelle_Click(object sender, EventArgs e)
        {
            if (conn.State == ConnectionState.Closed)
            {
                conn.Open();
            }
            string kayit = "update Book set BookName=@1,BookType=@2,BookAuthor=@3,PageNumber=@4, Publisher=@5,BookSummary=@6,YearPrint=@7,QR=@8, Durum=@9 where ID=@ID";
            SqlCommand komut = new SqlCommand(kayit, conn);
            komut.Parameters.AddWithValue("@1", txtKitapAdi.Text);
            komut.Parameters.AddWithValue("@2", cmbKitapTur.Text);
            komut.Parameters.AddWithValue("@3", txtKitapYazar.Text);
            komut.Parameters.AddWithValue("@4", txtSayfaSayisi.Text);
            komut.Parameters.AddWithValue("@5", txtYayinevi.Text);
            komut.Parameters.AddWithValue("@6", rtxtOzet.Text);
            komut.Parameters.AddWithValue("@7", txtBasimYili.Text);
            komut.Parameters.AddWithValue("@8", txtQR.Text);
            komut.Parameters.AddWithValue("@9", txtdrm.Text);
            komut.Parameters.AddWithValue("@ID", txtID.Text);

            komut.ExecuteNonQuery();
            conn.Close();
            MessageBox.Show("Kitap Bilgileri Güncellendi", "Başarılı", MessageBoxButtons.OK, MessageBoxIcon.Information);
            txtQR.Clear();
            txtKitapAdi.Clear();
            txtKitapYazar.Clear();
            txtSayfaSayisi.Clear();
            txtYayinevi.Clear();
            rtxtOzet.Clear();
            pcbQR.Visible = false;
            txtBasimYili.Clear();
            cmbKitapTur.Text = "Seçiniz";
            textBox2.Clear();
            lvKitap.Items.Clear();

            SqlCommand cmd = new SqlCommand("select * from Book where durum=0", conn);
            conn.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                ListViewItem item = new ListViewItem(dr["BookName"].ToString());
                item.SubItems.Add(dr["ID"].ToString());
                item.SubItems.Add(dr["BookType"].ToString());
                item.SubItems.Add(dr["BookAuthor"].ToString());
                item.SubItems.Add(dr["PageNumber"].ToString());
                item.SubItems.Add(dr["Publisher"].ToString());
                item.SubItems.Add(dr["BookSummary"].ToString());
                item.SubItems.Add(dr["YearPrint"].ToString());
                item.SubItems.Add(dr["QR"].ToString());
                lvKitap.Items.Add(item);
            }
            dr.Close();
            conn.Close();
        }
        private void PictureBox3_Click(object sender, EventArgs e)
        {
            new Menu().YetkiliID = yetkiliid;
            new Menu().Yetkilendirme();
            new Menu().Show();
            Hide();
        }
        private void PictureBox2_Click(object sender, EventArgs e)
        {
            this.WindowState = System.Windows.Forms.FormWindowState.Minimized;
        }
        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            SqlCommand cmd = new SqlCommand("select * from Book where QR = '"+textBox2.Text+"' durum=0", conn);
            conn.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                ListViewItem item = new ListViewItem(dr["BookName"].ToString());
                item.SubItems.Add(dr["ID"].ToString());
                item.SubItems.Add(dr["BookType"].ToString());
                item.SubItems.Add(dr["BookAuthor"].ToString());
                item.SubItems.Add(dr["PageNumber"].ToString());
                item.SubItems.Add(dr["Publisher"].ToString());
                item.SubItems.Add(dr["BookSummary"].ToString());
                item.SubItems.Add(dr["YearPrint"].ToString());
                item.SubItems.Add(dr["QR"].ToString());
                lvKitap.Items.Add(item);
            }
            dr.Close();
            conn.Close();
        }
        private void textBox2_TextChanged_1(object sender, EventArgs e)
        {
            lvKitap.Items.Clear();
            SqlCommand doldur = new SqlCommand("select * from Book where QR = '" + txtQR.Text + "'and Durum=0", conn);
            conn.Open();
            SqlDataReader dr = doldur.ExecuteReader();
            while (dr.Read())
            {
                ListViewItem item = new ListViewItem(dr["QR"].ToString());
                item.SubItems.Add(dr["BookName"].ToString());
                item.SubItems.Add(dr["BookAuthor"].ToString());
                item.SubItems.Add(dr["BookType"].ToString());
                item.SubItems.Add(dr["ID"].ToString());

                lvKitap.Items.Add(item);
            }
            dr.Close();
            conn.Close();
        }
        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            lvKitap.Items.Clear();

            SqlCommand doldur = new SqlCommand("select * from Book where BookName like '%" + textBox1.Text + "%'and Durum=0", conn);
            conn.Open();
            SqlDataReader dr = doldur.ExecuteReader();
            while (dr.Read())
            {
                ListViewItem item = new ListViewItem(dr["QR"].ToString());
                item.SubItems.Add(dr["BookName"].ToString());
                item.SubItems.Add(dr["BookAuthor"].ToString());
                item.SubItems.Add(dr["BookType"].ToString());
                item.SubItems.Add(dr["ID"].ToString());
                lvKitap.Items.Add(item);
            }
            dr.Close();
            conn.Close();
        }
        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox1.SelectedIndex == 0)
            {
                lvKitap.Items.Clear();
            }
            else if (comboBox1.SelectedIndex == 1)
            {
                lvKitap.Items.Clear();

                SqlCommand doldur = new SqlCommand("select * from Book where Durum=0", conn);
                conn.Open();
                SqlDataReader dr = doldur.ExecuteReader();
                while (dr.Read())
                {
                    ListViewItem item = new ListViewItem(dr["QR"].ToString());
                    item.SubItems.Add(dr["BookName"].ToString());
                    item.SubItems.Add(dr["BookAuthor"].ToString());
                    item.SubItems.Add(dr["BookType"].ToString());
                    item.SubItems.Add(dr["ID"].ToString());
                    lvKitap.Items.Add(item);
                }
                dr.Close();
                conn.Close();
            }
            else
            {
                lvKitap.Items.Clear();

                SqlCommand doldur = new SqlCommand("select * from Book where BookType =@1 and Durum=0", conn);
                conn.Open();
                doldur.Parameters.AddWithValue("@1",comboBox1);
                SqlDataReader dr = doldur.ExecuteReader();
                while (dr.Read())
                {
                    ListViewItem item = new ListViewItem(dr["QR"].ToString());
                    item.SubItems.Add(dr["BookName"].ToString());
                    item.SubItems.Add(dr["BookAuthor"].ToString());
                    item.SubItems.Add(dr["BookType"].ToString());
                    item.SubItems.Add(dr["ID"].ToString());
                    lvKitap.Items.Add(item);
                }
                dr.Close();
                conn.Close();
            }
        }
        public int ct2=0;
        private int yetkiliid;

        private void button3_Click(object sender, EventArgs e)
        {
            if (ct2 == 0)
            {
                groupBox5.Visible = true;
                ct2 = 1;
                button3.Text = "Kodla Ara";
            }
            else
            {
                groupBox5.Visible = false;
                ct2 = 0;
                button3.Text = "Detaylı Ara";
                textBox1.Text = "";
                cmbKitapTur.SelectedIndex = 0;
            }
        }

        private void txtdrm_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
