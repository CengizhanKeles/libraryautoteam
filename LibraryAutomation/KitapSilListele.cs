﻿using System;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Text;
using System.Windows.Forms;

namespace LibraryAutomation
{
    public partial class KitapSilListele : Form
    {
        SqlConnection conn = new SqlConnection(@"Data source=.\SQLEXPRESS;Database=Kutuphane;Integrated Security=true;");
        public KitapSilListele()
        {
            InitializeComponent();
        }

        private void PictureBox3_Click(object sender, EventArgs e)
        {
            new Menu().YetkiliID = yetkiliid;
            new Menu().Yetkilendirme();
            new Menu().Show();
            Hide();

        }

        private void PictureBox4_Click(object sender, EventArgs e)
        {
            this.WindowState = System.Windows.Forms.FormWindowState.Minimized;
        }
        int kitapSayisi = -1;
        private void KitapSilListele_Load(object sender, EventArgs e)
        {
            lvKitap.Items.Clear();
            // Kitap Sildir
            lvKitap.View = View.Details;
            lvKitap.GridLines = true;
            lvKitap.FullRowSelect = true;
            lvKitap.Columns.Add("Kitap Adı", 150);
            lvKitap.Columns.Add("Kitap Yazarı", 125);
            lvKitap.Columns.Add("Kitap Türü", 125);
            lvKitap.Columns.Add("ID", 1);


            lvKitap.AutoResizeColumns(ColumnHeaderAutoResizeStyle.None);




            /*  SqlCommand shw = new SqlCommand("select * from Book where Durum=0", conn);
              conn.Open();
              shw.ExecuteNonQuery();
              SqlDataReader dr = shw.ExecuteReader();
              while (dr.Read())
              {
                  ListViewItem item = new ListViewItem(dr["QR"].ToString());
                  item.SubItems.Add(dr["BookName"].ToString());
                  item.SubItems.Add(dr["BookAuthor"].ToString());
                  item.SubItems.Add(dr["BookType"].ToString());
                  item.SubItems.Add(dr["ID"].ToString());
                  lvKitap.Items.Add(item);
              }
              dr.Close();
              conn.Close();*/
        }
        bool formTasiniyor = false;
        Point baslangicNoktasi = new Point(0, 0);
        private void Panel3_MouseDown(object sender, MouseEventArgs e)
        {
            formTasiniyor = true;
            baslangicNoktasi = new Point(e.X, e.Y);
        }

        private void Panel3_MouseMove(object sender, MouseEventArgs e)
        {

            if (formTasiniyor)
            {
                Point p = PointToScreen(e.Location);
                Location = new Point(p.X - this.baslangicNoktasi.X, p.Y - this.baslangicNoktasi.Y);
            }
        }

        private void Panel3_MouseUp(object sender, MouseEventArgs e)
        {
            formTasiniyor = false;
        }



        private void CheckBox2_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox2.Checked)
            {
                foreach (ListViewItem item in lvKitap.Items)
                {
                    item.Checked = true;
                }
            }
            else
            {
                foreach (ListViewItem item in lvKitap.Items)
                {
                    item.Checked = false;
                }
            }
        }

        private void BtnKitapSil_Click(object sender, EventArgs e)
        {
            DialogResult Result = MessageBox.Show("Seçili Kitap Silinecektir Onaylıyormsunuz ?", "Dikkat", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (Result == DialogResult.Yes)
            {
                try
                {
                    foreach (ListViewItem item in lvKitap.CheckedItems)
                    {
                        SqlCommand update = new SqlCommand("Update Book set Durum=@1 where ID = '" + item.SubItems[4].Text + "'", conn);
                        conn.Open();
                        update.Parameters.AddWithValue("@1", 1);
                        update.ExecuteNonQuery();
                        conn.Close();
                    }





                    conn.Open();
                    lvKitap.Items.Clear();
                    SqlCommand cmd2 = new SqlCommand("Select * From Book where Durum=@1", conn);
                    cmd2.Parameters.AddWithValue("@1", 0);
                    SqlDataReader dr = cmd2.ExecuteReader();
                    while (dr.Read())
                    {
                        ListViewItem item = new ListViewItem(dr["BookName"].ToString());
                        item.SubItems.Add(dr["ID"].ToString());
                        item.SubItems.Add(dr["BookType"].ToString());
                        item.SubItems.Add(dr["BookAuthor"].ToString());
                        item.SubItems.Add(dr["PageNumber"].ToString());
                        item.SubItems.Add(dr["Publisher"].ToString());
                        item.SubItems.Add(dr["BookSummary"].ToString());
                        item.SubItems.Add(dr["YearPrint"].ToString());
                        item.SubItems.Add(dr["QR"].ToString());
                        lvKitap.Items.Add(item);
                    }
                    dr.Close();
                    conn.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Kitap Silinemedi" + ex, "Hata", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }



        private void LvKitap_ColumnWidthChanged(object sender, ColumnWidthChangedEventArgs e)
        {
            if (lvKitap.Columns[1].Width > 0)
            {
                lvKitap.Columns[1].Width = 0;
            }
        }

        private void LvKullanici_ColumnWidthChanged(object sender, ColumnWidthChangedEventArgs e)
        {
            if (lvKitap.Columns[1].Width > 0)
            {
                lvKitap.Columns[1].Width = 0;
            }
        }

        private void TextBox1_TextChanged(object sender, EventArgs e)
        {


        }





        private void LvKitap_ColumnWidthChanged_1(object sender, ColumnWidthChangedEventArgs e)
        {

            if (lvKitap.Columns[1].Width > 0)
            {
                lvKitap.Columns[1].Width = 0;
            }

        }

        private void lvKitap_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
        public int ct2 = 0;
        private int yetkiliid;

        private void button3_Click(object sender, EventArgs e)
        {
            if (ct2 == 0)
            {
                groupBox5.Visible = true;
                ct2 = 1;
                button3.Text = "Kodla Ara";



            }
            else
            {
                groupBox5.Visible = false;
                ct2 = 0;
                button3.Text = "Detaylı Ara";
                textBox1.Text = "";

                cmbKitapTur.SelectedIndex = 0;


            }
        }

        private void txtQR_TextChanged(object sender, EventArgs e)
        {
            lvKitap.Items.Clear();

            SqlCommand doldur = new SqlCommand("select * from Book where QR = '" + txtQR.Text + "'and Durum=0", conn);
            conn.Open();
            SqlDataReader dr = doldur.ExecuteReader();
            while (dr.Read())
            {
                ListViewItem item = new ListViewItem(dr["QR"].ToString());
                item.SubItems.Add(dr["BookName"].ToString());
                item.SubItems.Add(dr["BookAuthor"].ToString());
                item.SubItems.Add(dr["BookType"].ToString());
                item.SubItems.Add(dr["ID"].ToString());
                lvKitap.Items.Add(item);
            }
            dr.Close();
            conn.Close();
        }

        private void textBox1_TextChanged_1(object sender, EventArgs e)
        {
            lvKitap.Items.Clear();

            SqlCommand doldur = new SqlCommand("select * from Book where BookName like '%" + textBox1.Text + "%'and Durum=0", conn);
            conn.Open();
            SqlDataReader dr = doldur.ExecuteReader();
            while (dr.Read())
            {
                ListViewItem item = new ListViewItem(dr["QR"].ToString());
                item.SubItems.Add(dr["BookName"].ToString());
                item.SubItems.Add(dr["BookAuthor"].ToString());
                item.SubItems.Add(dr["BookType"].ToString());
                item.SubItems.Add(dr["ID"].ToString());
                lvKitap.Items.Add(item);
            }
            dr.Close();
            conn.Close();
        }

        private void cmbKitapTur_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbKitapTur.SelectedIndex == 0)
            {
                lvKitap.Items.Clear();
            }
            else if (cmbKitapTur.SelectedIndex == 1)
            {
                lvKitap.Items.Clear();

                SqlCommand doldur = new SqlCommand("select * from Book where Durum=0", conn);
                conn.Open();
                SqlDataReader dr = doldur.ExecuteReader();
                while (dr.Read())
                {
                    ListViewItem item = new ListViewItem(dr["QR"].ToString());
                    item.SubItems.Add(dr["BookName"].ToString());
                    item.SubItems.Add(dr["BookAuthor"].ToString());
                    item.SubItems.Add(dr["BookType"].ToString());
                    item.SubItems.Add(dr["ID"].ToString());

                    lvKitap.Items.Add(item);
                }
                dr.Close();
                conn.Close();
            }
            else
            {
                lvKitap.Items.Clear();

                SqlCommand doldur = new SqlCommand("select * from Book where BookType =@1 and Durum=0", conn);
                conn.Open();
                doldur.Parameters.AddWithValue("@1",cmbKitapTur.Text);
                SqlDataReader dr = doldur.ExecuteReader();
                while (dr.Read())
                {
                    ListViewItem item = new ListViewItem(dr["QR"].ToString());
                    item.SubItems.Add(dr["BookName"].ToString());
                    item.SubItems.Add(dr["BookAuthor"].ToString());
                    item.SubItems.Add(dr["BookType"].ToString());
                    item.SubItems.Add(dr["ID"].ToString());

                    lvKitap.Items.Add(item);
                }
                dr.Close();
                conn.Close();
            }
        }
    }
}


