﻿namespace LibraryAutomation
{
    partial class Sistem_Yoneticisi
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
  private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Sistem_Yoneticisi));
            this.PanelSol = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.button3 = new System.Windows.Forms.Button();
            this.txtUkad = new System.Windows.Forms.TextBox();
            this.cmbYetki = new System.Windows.Forms.ComboBox();
            this.txtUsifre = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.btnUserEkle = new System.Windows.Forms.Button();
            this.txtUsad = new System.Windows.Forms.TextBox();
            this.txtUad = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.PanelUst = new System.Windows.Forms.Panel();
            this.LblU = new System.Windows.Forms.Label();
            this.pictureKucult = new System.Windows.Forms.PictureBox();
            this.pictureCikis = new System.Windows.Forms.PictureBox();
            this.Gr = new System.Windows.Forms.GroupBox();
            this.listView1 = new System.Windows.Forms.ListView();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.Cmbsecim = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.PanelSol.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.PanelUst.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureKucult)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureCikis)).BeginInit();
            this.Gr.SuspendLayout();
            this.SuspendLayout();
            // 
            // PanelSol
            // 
            this.PanelSol.BackColor = System.Drawing.Color.SteelBlue;
            this.PanelSol.Controls.Add(this.pictureBox1);
            this.PanelSol.Controls.Add(this.groupBox2);
            this.PanelSol.Location = new System.Drawing.Point(-1, 35);
            this.PanelSol.Name = "PanelSol";
            this.PanelSol.Size = new System.Drawing.Size(262, 411);
            this.PanelSol.TabIndex = 2;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(3, 6);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(53, 49);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 50;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.button3);
            this.groupBox2.Controls.Add(this.txtUkad);
            this.groupBox2.Controls.Add(this.cmbYetki);
            this.groupBox2.Controls.Add(this.txtUsifre);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.btnUserEkle);
            this.groupBox2.Controls.Add(this.txtUsad);
            this.groupBox2.Controls.Add(this.txtUad);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.groupBox2.Location = new System.Drawing.Point(3, 61);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(266, 228);
            this.groupBox2.TabIndex = 49;
            this.groupBox2.TabStop = false;
            this.groupBox2.Visible = false;
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.SteelBlue;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.Font = new System.Drawing.Font("Calibri Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.button3.ForeColor = System.Drawing.Color.White;
            this.button3.Location = new System.Drawing.Point(18, 169);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(84, 49);
            this.button3.TabIndex = 50;
            this.button3.Text = "Temizle";
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Visible = false;
            this.button3.Click += new System.EventHandler(this.Button3_Click);
            // 
            // txtUkad
            // 
            this.txtUkad.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.txtUkad.Location = new System.Drawing.Point(108, 75);
            this.txtUkad.Name = "txtUkad";
            this.txtUkad.Size = new System.Drawing.Size(137, 22);
            this.txtUkad.TabIndex = 54;
            // 
            // cmbYetki
            // 
            this.cmbYetki.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbYetki.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.cmbYetki.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.cmbYetki.Items.AddRange(new object[] {
            "Seçiniz"});
            this.cmbYetki.Location = new System.Drawing.Point(109, 131);
            this.cmbYetki.Name = "cmbYetki";
            this.cmbYetki.Size = new System.Drawing.Size(136, 23);
            this.cmbYetki.TabIndex = 53;
            this.cmbYetki.SelectedIndexChanged += new System.EventHandler(this.CmbYetki_SelectedIndexChanged);
            // 
            // txtUsifre
            // 
            this.txtUsifre.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.txtUsifre.Location = new System.Drawing.Point(109, 103);
            this.txtUsifre.Name = "txtUsifre";
            this.txtUsifre.Size = new System.Drawing.Size(136, 22);
            this.txtUsifre.TabIndex = 52;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label8.Location = new System.Drawing.Point(58, 106);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(40, 16);
            this.label8.TabIndex = 51;
            this.label8.Text = "Şifre";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label5.Location = new System.Drawing.Point(5, 78);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(93, 16);
            this.label5.TabIndex = 50;
            this.label5.Text = "Kullanıcı Adı";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label4.Location = new System.Drawing.Point(55, 134);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(43, 16);
            this.label4.TabIndex = 49;
            this.label4.Text = "Yetki";
            // 
            // btnUserEkle
            // 
            this.btnUserEkle.BackColor = System.Drawing.Color.SteelBlue;
            this.btnUserEkle.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnUserEkle.Font = new System.Drawing.Font("Calibri Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnUserEkle.ForeColor = System.Drawing.Color.White;
            this.btnUserEkle.Location = new System.Drawing.Point(108, 169);
            this.btnUserEkle.Name = "btnUserEkle";
            this.btnUserEkle.Size = new System.Drawing.Size(136, 49);
            this.btnUserEkle.TabIndex = 48;
            this.btnUserEkle.Text = "Ekle";
            this.btnUserEkle.UseVisualStyleBackColor = false;
            this.btnUserEkle.Click += new System.EventHandler(this.btnUserEkle_Click);
            // 
            // txtUsad
            // 
            this.txtUsad.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.txtUsad.Location = new System.Drawing.Point(108, 47);
            this.txtUsad.Name = "txtUsad";
            this.txtUsad.Size = new System.Drawing.Size(136, 22);
            this.txtUsad.TabIndex = 44;
            // 
            // txtUad
            // 
            this.txtUad.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.txtUad.Location = new System.Drawing.Point(109, 19);
            this.txtUad.Name = "txtUad";
            this.txtUad.Size = new System.Drawing.Size(136, 22);
            this.txtUad.TabIndex = 43;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label6.Location = new System.Drawing.Point(45, 50);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(53, 16);
            this.label6.TabIndex = 40;
            this.label6.Text = "Soyad";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label7.Location = new System.Drawing.Point(71, 22);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(27, 16);
            this.label7.TabIndex = 39;
            this.label7.Text = "Ad";
            // 
            // PanelUst
            // 
            this.PanelUst.BackColor = System.Drawing.Color.SteelBlue;
            this.PanelUst.Controls.Add(this.LblU);
            this.PanelUst.Controls.Add(this.pictureKucult);
            this.PanelUst.Controls.Add(this.pictureCikis);
            this.PanelUst.Location = new System.Drawing.Point(0, 0);
            this.PanelUst.Name = "PanelUst";
            this.PanelUst.Size = new System.Drawing.Size(1063, 35);
            this.PanelUst.TabIndex = 3;
            // 
            // LblU
            // 
            this.LblU.AutoSize = true;
            this.LblU.Font = new System.Drawing.Font("Comic Sans MS", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblU.ForeColor = System.Drawing.SystemColors.Control;
            this.LblU.Location = new System.Drawing.Point(3, 2);
            this.LblU.Name = "LblU";
            this.LblU.Size = new System.Drawing.Size(272, 30);
            this.LblU.TabIndex = 17;
            this.LblU.Text = "LibraU Sistem Yöneticisi ";
            // 
            // pictureKucult
            // 
            this.pictureKucult.Image = ((System.Drawing.Image)(resources.GetObject("pictureKucult.Image")));
            this.pictureKucult.Location = new System.Drawing.Point(974, 1);
            this.pictureKucult.Name = "pictureKucult";
            this.pictureKucult.Size = new System.Drawing.Size(43, 34);
            this.pictureKucult.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureKucult.TabIndex = 29;
            this.pictureKucult.TabStop = false;
            // 
            // pictureCikis
            // 
            this.pictureCikis.Image = ((System.Drawing.Image)(resources.GetObject("pictureCikis.Image")));
            this.pictureCikis.Location = new System.Drawing.Point(1023, 0);
            this.pictureCikis.Name = "pictureCikis";
            this.pictureCikis.Size = new System.Drawing.Size(37, 35);
            this.pictureCikis.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureCikis.TabIndex = 28;
            this.pictureCikis.TabStop = false;
            this.pictureCikis.Click += new System.EventHandler(this.PictureCikis_Click);
            // 
            // Gr
            // 
            this.Gr.Controls.Add(this.listView1);
            this.Gr.Controls.Add(this.label3);
            this.Gr.Controls.Add(this.label2);
            this.Gr.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.Gr.Location = new System.Drawing.Point(267, 96);
            this.Gr.Name = "Gr";
            this.Gr.Size = new System.Drawing.Size(770, 252);
            this.Gr.TabIndex = 5;
            this.Gr.TabStop = false;
            this.Gr.Text = "Kullanıcılar";
            // 
            // listView1
            // 
            this.listView1.CheckBoxes = true;
            this.listView1.FullRowSelect = true;
            this.listView1.HideSelection = false;
            this.listView1.Location = new System.Drawing.Point(3, 27);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(774, 201);
            this.listView1.TabIndex = 0;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.Details;
            this.listView1.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.ListView1_ItemChecked);
            this.listView1.ItemSelectionChanged += new System.Windows.Forms.ListViewItemSelectionChangedEventHandler(this.ListView1_ItemSelectionChanged);
            this.listView1.SelectedIndexChanged += new System.EventHandler(this.ListView1_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(708, 1);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(20, 23);
            this.label3.TabIndex = 40;
            this.label3.Text = "0";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(510, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(205, 23);
            this.label2.TabIndex = 39;
            this.label2.Text = "Seçilen Kullanııcı Sayısı : ";
            // 
            // Cmbsecim
            // 
            this.Cmbsecim.BackColor = System.Drawing.SystemColors.Window;
            this.Cmbsecim.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cmbsecim.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.Cmbsecim.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.Cmbsecim.Items.AddRange(new object[] {
            "Seçiniz",
            "Öğrenci",
            "Öğretmen",
            "Sistem Yöneticisi"});
            this.Cmbsecim.Location = new System.Drawing.Point(442, 57);
            this.Cmbsecim.Name = "Cmbsecim";
            this.Cmbsecim.Size = new System.Drawing.Size(180, 24);
            this.Cmbsecim.TabIndex = 0;
            this.Cmbsecim.SelectedIndexChanged += new System.EventHandler(this.Cmbsecim_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label1.Location = new System.Drawing.Point(267, 58);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(169, 19);
            this.label1.TabIndex = 37;
            this.label1.Text = "Kullanıcı Yetkisi Seçiniz : ";
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.SteelBlue;
            this.button1.Enabled = false;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Location = new System.Drawing.Point(909, 58);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(142, 36);
            this.button1.TabIndex = 41;
            this.button1.Text = "Seçilen Kullanıcıları Sil";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.Button1_Click);
            // 
            // Sistem_Yoneticisi
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1063, 445);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Cmbsecim);
            this.Controls.Add(this.Gr);
            this.Controls.Add(this.PanelUst);
            this.Controls.Add(this.PanelSol);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Sistem_Yoneticisi";
            this.Text = "Sistem_Yonetcisi";
            this.Load += new System.EventHandler(this.Sistem_Yoneticisi_Load);
            this.PanelSol.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.PanelUst.ResumeLayout(false);
            this.PanelUst.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureKucult)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureCikis)).EndInit();
            this.Gr.ResumeLayout(false);
            this.Gr.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel PanelSol;
        private System.Windows.Forms.Panel PanelUst;
        private System.Windows.Forms.Label LblU;
        private System.Windows.Forms.PictureBox pictureKucult;
        private System.Windows.Forms.PictureBox pictureCikis;
        private System.Windows.Forms.GroupBox Gr;
        private System.Windows.Forms.ComboBox Cmbsecim;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnUserEkle;
        private System.Windows.Forms.TextBox txtUsad;
        private System.Windows.Forms.TextBox txtUad;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtUsifre;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtUkad;
        private System.Windows.Forms.ComboBox cmbYetki;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}